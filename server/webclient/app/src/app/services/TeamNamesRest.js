import CrudRest from "./CrudRest";

class TeamNamesRest extends CrudRest {

    constructor() {
        super(window.location.pathname + "api/teamNames");
    }

}

export default TeamNamesRest;
