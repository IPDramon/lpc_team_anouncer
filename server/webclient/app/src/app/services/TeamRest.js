import CrudRest from "./CrudRest";

class TeamRest extends CrudRest {

    constructor() {
        super(window.location.pathname + "api/teams");
    }

}

export default TeamRest;
