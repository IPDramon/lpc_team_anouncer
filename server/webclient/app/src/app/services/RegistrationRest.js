import CrudRest from "./CrudRest";

class RegistrationRest extends CrudRest {

    constructor() {
        super(window.location.pathname + "api/registrations");
    }

}

export default RegistrationRest;
