import Axios from "axios";
import CrudRest from "./CrudRest";

class PlayerRest extends CrudRest {

    constructor() {
        super(window.location.pathname + "api/tournaments");
    }

    startTournament = (tournament) => {
        return Axios.post(this.baseUrl + "/start/" + tournament.id);
    }

}

export default PlayerRest;
