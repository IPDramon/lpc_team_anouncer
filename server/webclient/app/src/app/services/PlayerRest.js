import Axios from "axios";
import CrudRest from "./CrudRest";

class PlayerRest extends CrudRest {

    constructor() {
        super(window.location.pathname + "api/players");
    }

    findUnregisteredPlayers = (tournamentId) => {
        return Axios.get(this.baseUrl + "/unregistered/" + tournamentId);
    }

}

export default PlayerRest;
