import React, { Component } from 'react';
import { withTranslation } from "react-i18next";
import Typography from "@material-ui/core/Typography";

import AllTable from '../../../commons/table/AllTable';
import { compose } from 'recompose';

import { Button, TextField } from '@material-ui/core';
import { withRouter } from 'react-router-dom';
import TeamNamesRest from '../../../services/TeamNamesRest';

class TeamNamesAll extends Component {

    state = {
        teamNames: [],
        currentInput: null,
        selected: undefined
    }

    columns = [];

    teamNamesRest = new TeamNamesRest();

    componentDidMount() {

        const t = this.props.t;

        this.columns = [
            { title: t('teamName.name'), field: 'name' }
        ];
        console.debug("TeamNames loaded");

        this.fetchAll();
    }

    fetchAll = () => {
        this.teamNamesRest.findAll().then(
            response => {
                this.setState({teamNames: response.data})
            }, err => console.error(err));
    }

    render() {

        const { teamNames, selected, currentInput } = this.state;
        const { t } = this.props;

        return (
            <React.Fragment>
                <Typography variant="h1" color="primary">{t('teamName.title')}</Typography>
                <div>
                    <TextField
                        value={currentInput !== null ? currentInput : ""}
                        onChange={this.handleInputChange}
                    />
                    <Button onClick={this.addName} style={{marginTop: 0}} color="primary">+</Button>
                    <Button onClick={() => this.teamNamesRest.delete(selected).then(this.fetchAll)} style={{marginTop: 0}} >{t('button.delete')}</Button>
                </div>
                <AllTable
                    entities={teamNames}
                    columns={this.columns}
                    selected={selected}
                    onRowClick={this.onRowClick} />
                
            </React.Fragment>
        );
    }

    onRowClick = (teamName) => {
        this.setState({selected: teamName});
    }

    handleInputChange = (event) => {
        const target = event.target;
        const value = target.value;

        let currentInput = this.state.currentInput;
        currentInput = value;

        this.setState({
            currentInput
        });

    }

    addName = () => {
        const {currentInput} = this.state;
        if (currentInput) {
            this.teamNamesRest.create({name: currentInput}).then(response => {
                this.fetchAll();
                this.setState({
                    currentInput: null
                });
            })
        }
    }

}

export default compose(withRouter, withTranslation())(TeamNamesAll);
