import React, { Component } from 'react';
import {Route} from 'react-router-dom';
import TeamNamesAll from './all/TeamNamesAll';

class TeamNamesMain extends Component {

  render() {

    return (
        <React.Fragment>
          <Route exact path="/teamNames" component={TeamNamesAll}/>
        </React.Fragment>
    );
  }
}

export default TeamNamesMain;
