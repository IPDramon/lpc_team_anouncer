import React, { Component } from 'react';
import { withTranslation } from "react-i18next";
import Typography from "@material-ui/core/Typography";
import { withStyles } from '@material-ui/core';

import HomeStyles from './HomeStyles';
import { compose } from 'recompose';

class HomeAll extends Component {

    render() {

        const { t, classes } = this.props;


        return (
            <React.Fragment>
                <Typography className={classes.grow1} variant="h1" color="primary">{t('home.title')}</Typography>
            </React.Fragment>
        );
    }

}

export default compose(withStyles(HomeStyles), withTranslation())(HomeAll);
