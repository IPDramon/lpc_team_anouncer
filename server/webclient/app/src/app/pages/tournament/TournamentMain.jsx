import React, { Component } from 'react';
import {Route} from 'react-router-dom';
import TournamentAll from "./all/TournamentAll";
import TournamentSingle from './single/TournamentSingle';
import TeamsAll from './teams/TeamsAll';
import TeamsUpdate from './teams/TeamsUpdate';

class TournamentMain extends Component {

  render() {

    return (
        <React.Fragment>
          <Route exact path="/tournaments" component={TournamentAll}/>
          <Route exact path="/tournaments/create" component={TournamentSingle}/>
          <Route exact path="/tournaments/update/:id" component={TournamentSingle}/>
          <Route exact path="/tournaments/:id/teams" component={TeamsAll}/>
          <Route exact path="/tournaments/:id/teams/:teamId/update" component={TeamsUpdate}/>
        </React.Fragment>
    );
  }
}

export default TournamentMain;
