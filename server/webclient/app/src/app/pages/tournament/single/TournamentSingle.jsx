import React, { Component } from 'react';
import { withRouter } from "react-router";
import { withTranslation } from "react-i18next";
import Typography from "@material-ui/core/Typography";

import AddUpdateForm from '../../../commons/form/AddUpdateForm';
import { compose } from 'recompose';

import TournamentRest from '../../../services/TournamentRest';

class TournamentSingle extends Component {

    state = {
        titleKey: "",
        tournament: {
            name: ""
        },
        isLoading: true
    }

    attributes = [
        { name: "name", type: "string" }
    ];

    tournamentRest = new TournamentRest();

    componentDidMount() {
        let updateResult = this.checkUpdate();
        this.submitFunction = updateResult.submitFunction;
        this.setState({ titleKey: updateResult.titleKey });
    }

    render() {

        const { tournament, isLoading } = this.state;
        const { t } = this.props;

        return (
            <React.Fragment>
                <Typography variant="h1" color="primary">{t('tournament.title')}</Typography>

                {!isLoading &&
                    <React.Fragment>
                        <AddUpdateForm
                            entity={tournament}
                            prefix="tournament"
                            attributes={this.attributes}
                            handleChange={this.handleChange}
                            handleSubmit={this.handleSubmit} />
                    </React.Fragment>
                }
            </React.Fragment>
        );
    }

    checkUpdate() {
        let func;
        let titleKey = "";
        if (this.props.match.params.id === undefined) {
            func = this.tournamentRest.create;
            titleKey = "form.create";
            this.setState({ isLoading: false })
        } else {
            this.tournamentRest.findById(this.props.match.params.id).then(response => this.setState({ tournament: response.data, isLoading: false }));
            func = this.tournamentRest.update;
            titleKey = "form.update";
        }
        return { submitFunction: func, titleKey: titleKey };
    };

    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        let tournament = this.state.tournament;
        tournament[name] = value;

        this.setState({
            tournament: tournament
        });
    };

    updatePlayer = (tournament) => {
        this.setState({ tournament });
    }

    handleSubmit = (event) => {

        //turn off page relaod
        event.preventDefault();

        let tournament = this.state.tournament;

        this.attributes.forEach(attribute => {
            if (attribute.type === "time") {
                let date = new Date("1970-01-01T" + this.state.tournament[attribute.name]);
                tournament[attribute.name] = date.toISOString();
            }
        });

        this.submitFunction(tournament).then(this.goBack);
    };

    goBack = () => {
        this.props.history.push("/tournaments");
    };

}

export default compose(withTranslation())(withRouter(TournamentSingle));
