import React, { Component } from 'react';
import { withRouter } from "react-router";
import { withTranslation } from "react-i18next";
import Typography from "@material-ui/core/Typography";

import AddUpdateForm from '../../../commons/form/AddUpdateForm';
import { compose } from 'recompose';
import TeamRest from '../../../services/TeamRest';


class TeamsUpdate extends Component {

    state = {
        titleKey: "",
        team: {
            name: ""
        },
        isLoading: true
    }

    attributes = [
        { name: "name", type: "string" }
    ];

    teamRest = new TeamRest();

    componentDidMount() {
        let updateResult = this.checkUpdate();
        this.submitFunction = updateResult.submitFunction;
        this.setState({ titleKey: updateResult.titleKey });
    }

    render() {

        const { team, isLoading } = this.state;
        const { t } = this.props;

        return (
            <React.Fragment>
                <Typography variant="h1" color="primary">{t('team.title.update')}</Typography>

                {!isLoading &&
                    <React.Fragment>
                        <AddUpdateForm
                            entity={team}
                            prefix="team"
                            attributes={this.attributes}
                            handleChange={this.handleChange}
                            handleSubmit={this.handleSubmit} />
                    </React.Fragment>
                }
            </React.Fragment>
        );
    }

    checkUpdate() {
        let func;
        let titleKey = "";
        this.teamRest.findById(this.props.match.params.teamId).then(response => this.setState({ team: response.data, isLoading: false }));
        func = this.teamRest.update;
        titleKey = "form.update";
        return { submitFunction: func, titleKey: titleKey };
    };

    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        let team = this.state.team;
        team[name] = value;

        this.setState({
            team: team
        });
    };

    handleSubmit = (event) => {

        //turn off page relaod
        event.preventDefault();

        let team = this.state.team;

        this.attributes.forEach(attribute => {
            if (attribute.type === "time") {
                let date = new Date("1970-01-01T" + this.state.team[attribute.name]);
                team[attribute.name] = date.toISOString();
            }
        });

        this.submitFunction(team).then(this.goBack);
    };

    goBack = () => {
        this.props.history.push("/tournaments/" + this.props.match.params.id + "/teams");
    };

}

export default compose(withTranslation())(withRouter(TeamsUpdate));
