import React, { Component } from 'react';
import { withTranslation } from "react-i18next";
import Typography from "@material-ui/core/Typography";

import AllTable from '../../../commons/table/AllTable';
import { compose } from 'recompose';

import TournamentRest from '../../../services/TournamentRest';
import { Button } from '@material-ui/core';
import { withRouter } from 'react-router-dom';

class TeamsAll extends Component {

    state = {
        teams: [],
        tournamentName: "",
        selected: undefined,
    }

    columns = [];

    tournamentRest = new TournamentRest();

    componentDidMount() {

        const t = this.props.t;

        this.columns = [
            { title: t('team.name'), field: 'name' },
            { title: t('team.attacker'), field: 'attacker.name' },
            { title: t('team.defender'), field: 'defender.name' },
        ];

        this.fetchAll();
    }

    fetchAll = () => {
        this.tournamentRest.findById(this.props.match.params.id).then(
            response => {
                this.setState({teams: response.data.teams, tournamentName: response.data.name})
            }, err => console.error(err));
    }

    render() {

        const { teams, selected, tournamentName } = this.state;
        const { t } = this.props;

        return (
            <React.Fragment>
                <Typography variant="h1" color="primary">{t('team.title', {tournamentName: tournamentName})}</Typography>
                <Button onClick={this.goToUpdate}>{t('button.update')}</Button>
                <AllTable
                    entities={teams}
                    columns={this.columns}
                    selected={selected}
                    onRowClick={this.onRowClick} />
                
            </React.Fragment>
        );
    }

    goToUpdate = () => {
        const { selected } = this.state;
        const { history } = this.props;
        if (this.state.selected) {
            history.push("/tournaments/" + this.props.match.params.id + "/teams/" + selected.id + "/update");
        }
    }

    onRowClick = (tournament) => {
        this.setState({selected: tournament});
    }

    startTournament = () => {
        const { selected } = this.state;
        if (selected) {
            this.tournamentRest.startTournament(selected).then(response => {
                console.debug("StartedTournament", response);
            })
        }
    }


}

export default compose(withTranslation())(withRouter(TeamsAll));
