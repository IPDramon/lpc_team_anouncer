import React, { Component } from 'react';
import {Route} from 'react-router-dom';
import PlayerAll from "./all/PlayerAll";
import PlayerSingle from './single/PlayerSingle';

class PlayerMain extends Component {

  render() {

    return (
        <React.Fragment>
          <Route exact path="/players" component={PlayerAll}/>
          <Route exact path="/players/create" component={PlayerSingle}/>
          <Route exact path="/players/update/:id" component={PlayerSingle}/>
        </React.Fragment>
    );
  }
}

export default PlayerMain;
