import React, { Component } from 'react';
import { ChromePicker as ColorPicker } from 'react-color';

class HueChanger extends Component {

    render() {

        const { setColorGenerator, color, partName } = this.props;

        return (
            <React.Fragment>
                <div>
                    <div>{partName}</div>
                    <ColorPicker color={color} onChange={setColorGenerator(partName)} />
                </div>
            </React.Fragment>
        );
    }

}

export default HueChanger;