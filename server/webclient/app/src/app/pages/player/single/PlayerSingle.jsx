import React, { Component } from 'react';
import { withRouter } from "react-router";
import { withTranslation } from "react-i18next";
import Typography from "@material-ui/core/Typography";

import AddUpdateForm from '../../../commons/form/AddUpdateForm';
import { compose } from 'recompose';

import PlayerRest from '../../../services/PlayerRest';
import SpriteColorer from './SpriteColorer';


class PlayerSingle extends Component {

    state = {
        titleKey: "",
        player: {
            name: "",
            gender: "MALE",
            spriteColoring: {
                hair: { r: 0, g: 0, b: 0 },
                shirt: { r: 0, g: 0, b: 0 },
                pants: { r: 0, g: 0, b: 0 },
                shoes: { r: 0, g: 0, b: 0 }
            }
        },
        isLoading: true
    }

    attributes = [
        { name: "name", type: "string" }
    ];

    playerRest = new PlayerRest();

    componentDidMount() {
        let updateResult = this.checkUpdate();
        this.submitFunction = updateResult.submitFunction;
        this.setState({ titleKey: updateResult.titleKey });
    }

    render() {

        const { player, isLoading } = this.state;
        const { t } = this.props;

        return (
            <React.Fragment>
                <Typography variant="h1" color="primary">{t('player.title')}</Typography>

                {!isLoading &&
                    <React.Fragment>
                        <SpriteColorer
                            player={player}
                            updatePlayer={this.updatePlayer} />
                        <AddUpdateForm
                            entity={player}
                            prefix="player"
                            attributes={this.attributes}
                            handleChange={this.handleChange}
                            handleSubmit={this.handleSubmit} />
                    </React.Fragment>
                }
            </React.Fragment>
        );
    }

    checkUpdate() {
        let func;
        let titleKey = "";
        if (this.props.match.params.id === undefined) {
            func = this.playerRest.create;
            titleKey = "form.create";
            this.setState({ isLoading: false })
        } else {
            this.playerRest.findById(this.props.match.params.id).then(response => this.setState({ player: response.data, isLoading: false }));
            func = this.playerRest.update;
            titleKey = "form.update";
        }
        return { submitFunction: func, titleKey: titleKey };
    };

    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        let player = this.state.player;
        player[name] = value;

        this.setState({
            player: player
        });
    };

    updatePlayer = (player) => {
        this.setState({ player });
    }

    handleSubmit = (event) => {

        //turn off page relaod
        event.preventDefault();

        let player = this.state.player;

        this.attributes.forEach(attribute => {
            if (attribute.type === "time") {
                let date = new Date("1970-01-01T" + this.state.player[attribute.name]);
                player[attribute.name] = date.toISOString();
            }
        });

        this.submitFunction(player).then(this.goBack);
    };

    goBack = () => {
        this.props.history.push("/players");
    };

}

export default compose(withTranslation())(withRouter(PlayerSingle));
