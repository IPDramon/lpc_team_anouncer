package de.lpc.rest.controller;

import de.lpc.persistence.entity.TeamName;
import de.lpc.service.impl.TeamNameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * TeamName RestController
 * Have a look at the RequestMapping!!!!!!
 */
@RestController
@RequestMapping("${rest.base-path}/teamNames")
public class TeamNameController {

    @Autowired
    private TeamNameService teamNameService;

    @GetMapping
    public List<TeamName> findAll() {
        return this.teamNameService.findAll();
    }

    @GetMapping(value = "/{id}")
    public TeamName findById(@PathVariable("id") Long id) {
        return this.teamNameService.findById(id);
    }

    /**
     * @param teamName
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TeamName save(@RequestBody TeamName teamName) {
        return this.teamNameService.saveOrUpdate(teamName);
    }

    @PutMapping
    @ResponseStatus(value = HttpStatus.OK)
    public TeamName update(@RequestBody TeamName teamName) {
        return this.teamNameService.saveOrUpdate(teamName);
    }

    @DeleteMapping
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@RequestBody TeamName teamName) {
        this.teamNameService.delete(teamName);
    }

}
