package de.lpc.rest.controller;

import de.lpc.dto.PlayersByPreference;
import de.lpc.persistence.entity.Player;
import de.lpc.service.impl.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Player RestController
 * Have a look at the RequestMapping!!!!!!
 */
@RestController
@RequestMapping("${rest.base-path}/players")
public class PlayerController {

    @Autowired
    private PlayerService playerService;

    @GetMapping
    public List<Player> findAll() {
        return this.playerService.findAll();
    }

    @GetMapping(value = "/{id}")
    public Player findById(@PathVariable("id") Long id) {
        return this.playerService.findById(id);
    }

    /**
     * @param player
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Player save(@RequestBody Player player) {
        return this.playerService.saveOrUpdate(player);
    }

    @PutMapping
    @ResponseStatus(value = HttpStatus.OK)
    public Player update(@RequestBody Player player) {
        return this.playerService.saveOrUpdate(player);
    }

    @DeleteMapping
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@RequestBody Player player) {
        this.playerService.delete(player);
    }

    @GetMapping(value = "/tournament/{id}")
    @ResponseStatus(HttpStatus.OK)
    public PlayersByPreference playersByPreference(@PathVariable("id") Long id) {
        return this.playerService.findPlayersByPreference(id);
    }

    @GetMapping(value = "/unregistered/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<Player> unregisteredPlayers(@PathVariable("id") Long tournamentId) {
        return this.playerService.findUnregisteredPlayersForTournament(tournamentId);
    }

}
