package de.lpc.rest.controller;

import de.lpc.persistence.entity.Team;
import de.lpc.service.impl.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Team RestController
 * Have a look at the RequestMapping!!!!!!
 */
@RestController
@RequestMapping("${rest.base-path}/teams")
public class TeamController {

    @Autowired
    private TeamService teamService;

    @GetMapping
    public List<Team> findAll() {
        return this.teamService.findAll();
    }

    @GetMapping(value = "/{id}")
    public Team findById(@PathVariable("id") Long id) {
        return this.teamService.findById(id);
    }

    @PutMapping
    @ResponseStatus(value = HttpStatus.OK)
    public Team update(@RequestBody Team team) {
        return this.teamService.saveOrUpdate(team);
    }

    @DeleteMapping
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@RequestBody Team team) {
        this.teamService.delete(team);
    }

}
