package de.lpc.rest.controller;

import de.lpc.persistence.entity.RolePreference;
import de.lpc.service.impl.RolePreferenceService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * RolePreference RestController
 * Have a look at the RequestMapping!!!!!!
 */
@RestController
@RequestMapping("${rest.base-path}/rolePreferences")
public class RolePreferenceController {

    @Autowired
    private RolePreferenceService rolePreferenceService;

    @GetMapping
    public List<RolePreference> findAll() {
        return this.rolePreferenceService.findAll();
    }

}
