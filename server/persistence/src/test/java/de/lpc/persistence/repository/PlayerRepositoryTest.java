package de.lpc.persistence.repository;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;

import de.lpc.persistence.entity.Player;
import de.lpc.persistence.entity.RolePreference;
import de.lpc.persistence.entity.Tournament;
import org.junit.Assert;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PlayerRepositoryTest {

    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    TournamentRepository tournamentRepository;

    @Test
    @SqlGroup({
        @Sql(scripts = "classpath:createColor.sql"),
        @Sql(scripts = "classpath:createSpriteColoring.sql"),
        @Sql(scripts = "classpath:createPlayer.sql"),
        @Sql(scripts = "classpath:createTournament.sql"),
        @Sql(scripts = "classpath:createRegistration.sql")
    })
    public void playerByPreferenceTest() {

        Tournament tournament = tournamentRepository.findById(1L).orElseThrow(() -> new EntityNotFoundException(String.valueOf(1L)));

        List<Player> attackers = playerRepository.findByPreferenceForTournament(tournament, RolePreference.ATTACK);
        List<Player> defenders = playerRepository.findByPreferenceForTournament(tournament, RolePreference.DEFENSE);
        List<Player> neutrals = playerRepository.findByPreferenceForTournament(tournament, RolePreference.NONE);

        Assert.assertEquals(1, attackers.size());
        Assert.assertEquals(2, defenders.size());
        Assert.assertEquals(3, neutrals.size());

    }

    @Test
    @SqlGroup({
        @Sql(scripts = "classpath:createColor.sql"),
        @Sql(scripts = "classpath:createSpriteColoring.sql"),
        @Sql(scripts = "classpath:createPlayer.sql"),
        @Sql(scripts = "classpath:createTournament.sql"),
        @Sql(scripts = "classpath:createRegistration.sql")
    })
    public void unregisteredPlayersTest() {

        Tournament tournament = tournamentRepository.findById(1L).orElseThrow(() -> new EntityNotFoundException(String.valueOf(1L)));
        List<Player> unregisteredPlayers = playerRepository.findUnregistredForTournament(tournament);
        Assert.assertEquals(0, unregisteredPlayers.size());

        tournament = tournamentRepository.findById(2L).orElseThrow(() -> new EntityNotFoundException(String.valueOf(2L)));
        unregisteredPlayers = playerRepository.findUnregistredForTournament(tournament);
        Assert.assertEquals(6, unregisteredPlayers.size());
    }

}