CREATE TABLE IF NOT EXISTS `TEAM_NAME`
(
    `ID`      bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `NAME` varchar(255),
    `CREATED_ON` datetime,
    `UPDATED_ON` datetime
);