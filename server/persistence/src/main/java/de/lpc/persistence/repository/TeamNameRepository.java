package de.lpc.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import de.lpc.persistence.entity.TeamName;

/**
 * Color Repository class
 */
@Repository
public interface TeamNameRepository extends JpaRepository<TeamName, Long> {

}
