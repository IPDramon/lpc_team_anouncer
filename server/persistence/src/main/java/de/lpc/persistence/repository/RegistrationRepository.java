package de.lpc.persistence.repository;

import de.lpc.persistence.entity.Registration;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Color Repository class
 */
@Repository
public interface RegistrationRepository extends JpaRepository<Registration, Long> {

}
