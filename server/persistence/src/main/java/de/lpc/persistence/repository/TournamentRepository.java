package de.lpc.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import de.lpc.persistence.entity.Tournament;

/**
 * Color Repository class
 */
@Repository
public interface TournamentRepository extends JpaRepository<Tournament, Long> {

}
