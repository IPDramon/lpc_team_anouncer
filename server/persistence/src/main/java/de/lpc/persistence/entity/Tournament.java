package de.lpc.persistence.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Tournament Entity class
 */
@Entity
@Table(name = "TOURNAMENT")
public class Tournament extends AbstractEntity<Long> {

    //domain attributes
    @Column(name="NAME")
	private String name;
    
    @OneToMany(mappedBy = "tournament", cascade = CascadeType.PERSIST)
    private List<Team> teams = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "tournament", orphanRemoval = true)
    private List<Registration> registrations = new ArrayList<>();
    

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Team> getTeams() {
        return this.teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public List<Registration> getRegistrations() {
        return this.registrations;
    }

    public void setRegistrations(List<Registration> registrations) {
        this.registrations = registrations;
    }

    public Tournament name(String name) {
        this.name = name;
        return this;
    }

    public Tournament teams(List<Team> teams) {
        this.teams = teams;
        return this;
    }

    public Tournament registrations(List<Registration> registrations) {
        this.registrations = registrations;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Tournament)) {
            return false;
        }
        Tournament tournament = (Tournament) o;
        return Objects.equals(name, tournament.name) && Objects.equals(teams, tournament.teams) && Objects.equals(registrations, tournament.registrations);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, teams, registrations);
    }

    @Override
    public String toString() {
        return "{" +
            " name='" + getName() + "'" +
            ", teams='" + getTeams() + "'" +
            ", registrations='" + getRegistrations() + "'" +
            "}";
    }

}
