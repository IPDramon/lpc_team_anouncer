package de.lpc.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "TEAM_NAME")
public class TeamName extends AbstractEntity<Long> {
  
  @Column(name="NAME")
  private String name;

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }
  

}
