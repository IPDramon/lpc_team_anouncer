package de.lpc.persistence.entity;

import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * Player Entity class
 */
@Entity
@Table(name = "PLAYER")
public class Player extends AbstractEntity<Long> {

    //domain attributes
    @Column(name="NAME")
	private String name;
	
    @Column(name="GENDER")
    @Enumerated(EnumType.STRING)
    private Gender gender;
    
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="SPRITE_COLORING")
    private SpriteColoring spriteColoring;	

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return this.gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public SpriteColoring getSpriteColoring() {
        return this.spriteColoring;
    }

    public void setSpriteColoring(SpriteColoring spriteColoring) {
        this.spriteColoring = spriteColoring;
    }
    

    public Player name(String name) {
        this.name = name;
        return this;
    }

    public Player gender(Gender gender) {
        this.gender = gender;
        return this;
    }

    public Player spriteColoring(SpriteColoring spriteColoring) {
        this.spriteColoring = spriteColoring;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Player)) {
            return false;
        }
        Player player = (Player) o;
        return Objects.equals(name, player.name) && Objects.equals(gender, player.gender) && Objects.equals(spriteColoring, player.spriteColoring);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, gender, spriteColoring);
    }

}
