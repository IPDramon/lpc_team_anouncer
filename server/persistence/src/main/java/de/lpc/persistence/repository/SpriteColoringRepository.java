package de.lpc.persistence.repository;

import de.lpc.persistence.entity.SpriteColoring;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * SpriteColoring Repository class
 */
@Repository
public interface SpriteColoringRepository extends JpaRepository<SpriteColoring, Long> {

}
