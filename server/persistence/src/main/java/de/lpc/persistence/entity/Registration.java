package de.lpc.persistence.entity;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Player Entity class
 */
@Entity
@Table(name = "REGISTRATION")
public class Registration extends AbstractEntity<Long> {

    //domain attributes
    @Column(name="ROLE_PREFERENCE")
    @Enumerated(EnumType.STRING)
	private RolePreference rolePreference;
	
    @ManyToOne
    @JoinColumn(name="PLAYER")
    private Player player;

    @ManyToOne
    @JoinColumn(name="TOURNAMENT")
    private Tournament tournament;
    

    public RolePreference getRolePreference() {
        return this.rolePreference;
    }

    public void setRolePreference(RolePreference role) {
        this.rolePreference = role;
    }

    public Player getPlayer() {
        return this.player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Tournament getTournament() {
        return this.tournament;
    }

    public void setTournament(Tournament tournament) {
        this.tournament = tournament;
    }

    public Registration role(RolePreference role) {
        this.rolePreference = role;
        return this;
    }

    public Registration player(Player player) {
        this.player = player;
        return this;
    }

    public Registration tournament(Tournament tournament) {
        this.tournament = tournament;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Registration)) {
            return false;
        }
        Registration registration = (Registration) o;
        return Objects.equals(rolePreference, registration.rolePreference) && Objects.equals(player, registration.player) && Objects.equals(tournament, registration.tournament);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rolePreference, player, tournament);
    }

    @Override
    public String toString() {
        return "{" +
            " role='" + getRolePreference() + "'" +
            ", player='" + getPlayer() + "'" +
            ", tournament='" + getTournament() + "'" +
            "}";
    }

    
}
