package de.lpc.service.impl;

import de.lpc.dto.PlayersByPreference;
import de.lpc.persistence.entity.Player;
import de.lpc.persistence.entity.Team;
import de.lpc.persistence.entity.TeamName;
import de.lpc.persistence.entity.Tournament;
import de.lpc.persistence.repository.TournamentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 
 * Tournament Service class
 *
 */
@Service
public class TournamentService {

    @Autowired
    private TournamentRepository tournamentRepository;

    @Autowired
    private PlayerService playerService;

    @Autowired
    private TeamNameService teamNameService;

    @Autowired
    private TeamService teamService;

    /**
     * @return
     */
    public List<Tournament> findAll() {
        return this.tournamentRepository.findAll();
    }

    /**
     * @param id
     * @return
     */
    public Tournament findById(Long id) {
        return this.tournamentRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(String.valueOf(id)));
    }

    /**
     * @param tournament
     * @return
     */
    public Tournament saveOrUpdate(Tournament tournament) {
        return this.tournamentRepository.save(tournament);
    }

    /**
     * @param tournament
     */
    public void delete(Tournament tournament) {
        this.tournamentRepository.delete(tournament);
    }

    /**
     * creates the random teams for a tournament
     * If teams already exist they will not be generated newly
     * @param tournamentId
     */
    public void createRandomTeams(Long tournamentId) {
        Tournament tournament = this.findById(tournamentId);

        if (tournament.getTeams().isEmpty()) {
            PlayersByPreference playersByPreference = playerService.findPlayersByPreference(tournamentId);

            List<Player> neutrals = playersByPreference.getNeutrals();
            List<Player> attackers = playersByPreference.getAttackers();
            List<Player> defenders = playersByPreference.getDefenders();
            List<TeamName> teamNames = teamNameService.findAll();
    
            Collections.shuffle(neutrals);
            Collections.shuffle(teamNames);
            
            for (Player player: neutrals) {
                if (attackers.size() <= defenders.size()) {
                    attackers.add(player);
                } else {
                    defenders.add(player);
                }
            }
                
            Collections.shuffle(attackers);
            Collections.shuffle(defenders);
    
            while (attackers.size() < defenders.size() &&
                    Math.abs(attackers.size() - defenders.size()) > 1) {
                attackers.add(popPlayerFromList(defenders));
            }
    
            while (defenders.size() < attackers.size() &&
                    Math.abs(attackers.size() - defenders.size()) > 1) {
                defenders.add(popPlayerFromList(attackers));
            }
    
            int counter = 0;
            List<Team> teams = new ArrayList<Team>();
            while (!defenders.isEmpty()) {
                Team team = new Team();
                team.setTournament(tournament);
                team.setAttacker(popPlayerFromList(attackers));
                team.setDefender(popPlayerFromList(defenders));
                TeamName teamName = teamNames.get(0);
                teamNames.remove(teamName);
    
                String suffix = counter < teamNames.size() ? "" : String.valueOf((counter / teamNames.size()) + 1);
    
                team.setName(teamName.getName() + suffix);
                teamNames.add(teamName);
                team = teamService.saveOrUpdate(team);
                teams.add(team);
                counter++;
            }
            tournament.setTeams(teams);
    
            this.saveOrUpdate(tournament);
        }

    }

    private Player popPlayerFromList(List<Player> players) {
        Player player = players.get(0);
        players.remove(player);
        return player;
    }

}
