package de.lpc.service.impl;

import de.lpc.persistence.entity.Registration;
import de.lpc.persistence.repository.RegistrationRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

import java.util.List;

/**
 * 
 * Registration Service class
 *
 */
@Service
public class RegistrationService {

    @Autowired
    private RegistrationRepository registrationRepository;

    @Autowired
    private TournamentService tournamentService;
    @Autowired
    private PlayerService playerService;

    /**
     * @return
     */
    public List<Registration> findAll() {
        return this.registrationRepository.findAll();
    }

    /**
     * @param id
     * @return
     */
    public Registration findById(Long id) {
        return this.registrationRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(String.valueOf(id)));
    }

    /**
     * @param registration
     * @return
     */
    public Registration save(Registration registration) {
        return this.registrationRepository.save(registration);
    }

    /**
     * @param registration
     * @return
     */
    public Registration update(Registration registration) {
        Registration willBeUpdated = this.findById(registration.getId());
        willBeUpdated.setTournament(tournamentService.findById(registration.getTournament().getId()));
        willBeUpdated.setPlayer(playerService.findById(registration.getPlayer().getId()));
        willBeUpdated.setRolePreference(registration.getRolePreference());

        return this.registrationRepository.save(registration);
    }

    /**
     * @param registration
     */
    public void delete(Registration registration) {
        this.registrationRepository.delete(registration);
    }

}
