package de.lpc.service.impl;

import de.lpc.persistence.entity.Color;
import de.lpc.persistence.repository.ColorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

/**
 * 
 * Color Service class
 *
 */
@Service
public class ColorService {

    @Autowired
    private ColorRepository colorRepository;

    /**
     * @return
     */
    public List<Color> findAll() {
        return this.colorRepository.findAll();
    }

    /**
     * @param id
     * @return
     */
    public Color findById(Long id) {
        return this.colorRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(String.valueOf(id)));
    }

    /**
     * @param Color
     * @return
     */
    public Color saveOrUpdate(Color Color) {
        return this.colorRepository.save(Color);
    }

    /**
     * @param Color
     */
    public void delete(Color Color) {
        this.colorRepository.delete(Color);
    }

}
