package de.lpc.service.impl;

import de.lpc.persistence.entity.TeamName;
import de.lpc.persistence.repository.TeamNameRepository;
import de.lpc.persistence.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

/**
 * 
 * Team Service class
 *
 */
@Service
public class TeamNameService {

    @Autowired
    private TeamNameRepository teamNameRepository;

    /**
     * @return
     */
    public List<TeamName> findAll() {
        return this.teamNameRepository.findAll();
    }

    /**
     * @param id
     * @return
     */
    public TeamName findById(Long id) {
        return this.teamNameRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(String.valueOf(id)));
    }

    /**
     * @param teamName
     * @return
     */
    public TeamName saveOrUpdate(TeamName teamName) {
        return this.teamNameRepository.save(teamName);
    }

    /**
     * @param teamName
     */
    public void delete(TeamName teamName) {
        this.teamNameRepository.delete(teamName);
    }

}
