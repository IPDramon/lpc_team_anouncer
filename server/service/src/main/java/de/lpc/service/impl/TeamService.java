package de.lpc.service.impl;

import de.lpc.persistence.entity.Team;
import de.lpc.persistence.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

/**
 * 
 * Team Service class
 *
 */
@Service
public class TeamService {

    @Autowired
    private TeamRepository teamRepository;

    /**
     * @return
     */
    public List<Team> findAll() {
        return this.teamRepository.findAll();
    }

    /**
     * @param id
     * @return
     */
    public Team findById(Long id) {
        return this.teamRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(String.valueOf(id)));
    }

    /**
     * @param team
     * @return
     */
    public Team saveOrUpdate(Team team) {
        if (team.getId() != null) {
            String newName = team.getName();
            team = this.findById(team.getId());
            team.setName(newName);
        }
        return this.teamRepository.save(team);
    }

    /**
     * @param team
     */
    public void delete(Team team) {
        this.teamRepository.delete(team);
    }

}
