package de.lpc.dto;

import java.util.Objects;
import java.util.List;
import java.util.ArrayList;

import de.lpc.persistence.entity.Player;


public class PlayersByPreference {

    private List<Player> attackers = new ArrayList<>();
    private List<Player> defenders = new ArrayList<>();
    private List<Player> neutrals = new ArrayList<>();

    public List<Player> getAttackers() {
        return this.attackers;
    }

    public void setAttackers(List<Player> attackers) {
        this.attackers = attackers;
    }

    public List<Player> getDefenders() {
        return this.defenders;
    }

    public void setDefenders(List<Player> defenders) {
        this.defenders = defenders;
    }

    public List<Player> getNeutrals() {
        return this.neutrals;
    }

    public void setNeutrals(List<Player> neutrals) {
        this.neutrals = neutrals;
    }

    public PlayersByPreference attackers(List<Player> attackers) {
        this.attackers = attackers;
        return this;
    }

    public PlayersByPreference defenders(List<Player> defenders) {
        this.defenders = defenders;
        return this;
    }

    public PlayersByPreference neutrals(List<Player> neutrals) {
        this.neutrals = neutrals;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof PlayersByPreference)) {
            return false;
        }
        PlayersByPreference playersByPreference = (PlayersByPreference) o;
        return Objects.equals(attackers, playersByPreference.attackers) && Objects.equals(defenders, playersByPreference.defenders) && Objects.equals(neutrals, playersByPreference.neutrals);
    }

    @Override
    public int hashCode() {
        return Objects.hash(attackers, defenders, neutrals);
    }

    @Override
    public String toString() {
        return "{" +
            " attackers='" + getAttackers() + "'" +
            ", defenders='" + getDefenders() + "'" +
            ", neutrals='" + getNeutrals() + "'" +
            "}";
    }


}
