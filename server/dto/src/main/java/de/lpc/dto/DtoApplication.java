package de.lpc.dto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main SpringApplication to start the whole project
 */
@SpringBootApplication
public class DtoApplication {

    public static void main(String[] args) {
        SpringApplication.run(new Class[]{DtoApplication.class}, args);
    }

}
