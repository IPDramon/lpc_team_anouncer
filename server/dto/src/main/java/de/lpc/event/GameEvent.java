package de.lpc.event;

import org.springframework.context.ApplicationEvent;

import de.lpc.dto.GameDTO;

public class GameEvent extends ApplicationEvent {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    
    private GameDTO gameDTO;
 
    public GameEvent(Object source, GameDTO gameDTO) {
        super(source);
        this.gameDTO = gameDTO;
    }
 
    /**
     * @return the gameDTO
     */
    public GameDTO getGameDTO() {
        return gameDTO;
    }
}