extends MarginContainer

const Team = preload("res://tournamentLoader/Team.gd")

var tournaments = []
var currentIndex = 0

func _ready():
	var url = globals.serverAddress + "/api/tournaments"
	$GetTournaments.request(url, [], false, HTTPClient.METHOD_GET, "")

	
func _on_StartButton_pressed():
	globals.teams = tournaments[currentIndex].teams
	var error = get_tree().change_scene("res://tournamentLoader/TournamentLoader.tscn")
	if (error):
		print("Error on switch to TournamentLoader: " + error)
	

func _on_BackButton_pressed():
	var error = get_tree().change_scene("res://mainmenu/MainMenu.tscn")
	if (error):
		print("Error on switch to MainMenu: " + error)


func _on_CheckButton_pressed():
	if $VBoxContainer/CheckButton.pressed:
		globals.tournamentMode = "DoubleElimination"
	else:
		globals.tournamentMode = "KO"


# warning-ignore:unused_argument
# warning-ignore:unused_argument
# warning-ignore:unused_argument
func _on_GetTournaments_request_completed(result, response_code, headers, body):
	tournaments = JSON.parse(body.get_string_from_utf8()).result
	for tournament in tournaments:
		var convertedTeams = []
		for team in tournament.teams:
			var convertedTeam = Team.new()
			convertedTeam.name = team.name
			convertedTeam.attack = team.attacker
			convertedTeam.defense = team.defender
			convertedTeams.append(convertedTeam)
		tournament.teams = convertedTeams
		$VBoxContainer/HBoxContainer/TournamentOptions.add_item(tournament.name)

func _on_TournamentOptions_item_selected(ID):
	currentIndex = ID
