extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
export(float) var runMultiplier = 1
var running = false

func _ready():
	$RunPlayerMale.set_run_multiplier(self.runMultiplier)

func start():
	running = true
	
func stop():
	running = false

func _process(delta):
	# Called every frame. Delta is time since last frame.
	# Update game logic here.
	if running:
		var player_pos = $RunPlayerMale.get_position()
		var label_pos = $PlayerNameLabel.rect_position
		var label_size = $PlayerNameLabel.rect_size
		
		if player_pos.x > label_pos.x:
			var visibility = 0
			if player_pos.x > 0:
				visibility = max(0, (1.0 * player_pos.x - label_pos.x) / label_size.x - 0.05)
			$PlayerNameLabel.percent_visible = min(1, visibility)
		
		
func reset_run_player(player):
	$PlayerNameLabel.percent_visible = 0
	$PlayerNameLabel.text = player.name
	var size = $PlayerNameLabel.rect_size;
	size.x = 20 * player.name.length()
	$PlayerNameLabel.rect_size = size
	$RunPlayerMale.set_position(Vector2(-32,0))
	$RunPlayerMale.show()
	$RunPlayerMale.recolor(player.spriteColoring)
	$RunPlayerMale.set_gender(player.gender)
	
