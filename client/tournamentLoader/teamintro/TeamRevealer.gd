extends Area2D

var countdown = 2
var teams = []
var currentTeam = 0

signal team_shown(team)
signal last_team_shown

func start(teams):
	self.visible = true
	self.teams = teams
	set_team(teams[currentTeam])
	if (teams.size() > 0):
		$NameRevealer1.start()
		$NameRevealer2.start()
	else:
		print("Error while showing teams -> no team found")

func _on_TeamRevealer_body_entered(body):
	countdown -= 1
	
	if self.visible and countdown < 1:
		emit_signal("team_shown", teams[currentTeam])
		countdown = 2
		currentTeam += 1
		if currentTeam < teams.size():
			var team = teams[currentTeam]
			set_team(team)
		if currentTeam >= teams.size():
			$NameRevealer1.stop()
			$NameRevealer2.stop()
			emit_signal("last_team_shown")

func set_team(team):
	$NameRevealer1.reset_run_player(team.attack)
	$NameRevealer2.reset_run_player(team.defense)
	$TeamNameShield.setTeamName(str(currentTeam + 1) + ": " + str(team.name))
