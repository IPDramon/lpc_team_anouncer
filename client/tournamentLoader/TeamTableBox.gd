extends VBoxContainer

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func display_team(team):
	$Player1.text = team.attack.name
	$Player2.text = team.defense.name
	$TeamName.text = " " + team.name
	$Divider.text = "----------"
