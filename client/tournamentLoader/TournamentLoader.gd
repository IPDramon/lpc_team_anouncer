extends Node2D

const TeamTableBox = preload("res://tournamentLoader/TeamTableBox.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	$TeamRevealer.start(globals.teams)


func _on_TeamRevealer_team_shown(team):
	var tTB = TeamTableBox.instance()
	tTB.display_team(team)
	$ScrollContainer/TeamsBox.add_child(tTB)


func _on_NextButton_pressed():
	if globals.tournamentMode == "KO":
		var error = get_tree().change_scene("res://tournament/KOTournament.tscn")
		if error:
			print(error)
	elif globals.tournamentMode == "DoubleElimination":
		var error = get_tree().change_scene("res://tournament/DoubleEliminationTournament.tscn")
		if error:
			print(error)


func _on_TeamRevealer_last_team_shown():
	$NextButton.visible = true

