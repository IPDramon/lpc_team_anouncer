extends Node

const Team = preload("res://tournamentLoader/Team.gd")

var teams = []
const teamNamesDir = "user://teamTournamentAnnouncer/"
export var teamNamesFilePath = teamNamesDir + "teamNames.txt"

var tournamentMode = "DoubleElimination"

var tournamentWinner = null

var serverAddress = ""
const serverAddressDir = "user://teamTournamentAnnouncer/"
export var serverAddressFilePath = serverAddressDir + "serverAddress.txt"
