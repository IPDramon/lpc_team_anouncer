extends MarginContainer

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	var dir = Directory.new()
	if not dir.dir_exists(globals.teamNamesDir):
		dir.make_dir_recursive(globals.teamNamesDir)
	var saveFile = File.new()
	if saveFile.file_exists(globals.teamNamesFilePath):
		saveFile.open(globals.teamNamesFilePath, File.READ)
		var saveFileContent = ""
		while not saveFile.eof_reached():
			saveFileContent = saveFileContent + saveFile.get_line()
		saveFile.close()
		var teamNames = parse_json(saveFileContent)
		print(teamNames)
		$VBoxContainer/NameManager.reset()
		$VBoxContainer/NameManager.reload_players(teamNames["teamNames"])


func _on_SaveButton_pressed():
	var teamNamesSaveDict = {
			"teamNames": $VBoxContainer/NameManager.players
		}
	var saveFile = File.new()
	saveFile.open(globals.teamNamesFilePath, File.WRITE)
	var saveJson = to_json(teamNamesSaveDict)
	saveFile.store_line(saveJson)
	saveFile.close()
	_on_BackButton_pressed()


func _on_BackButton_pressed():
	var error = get_tree().change_scene("res://mainmenu/MainMenu.tscn")
	if (error):
		print("Error on switch to MainMenu: " + error)
