extends MarginContainer

# Called when the node enters the scene tree for the first time.
func _ready():
	var dir = Directory.new()
	if not dir.dir_exists(globals.serverAddressDir):
		dir.make_dir_recursive(globals.serverAddressDir)
	var saveFile = File.new()
	if saveFile.file_exists(globals.serverAddressFilePath):
		saveFile.open(globals.serverAddressFilePath, File.READ)
		var saveFileContent = ""
		while not saveFile.eof_reached():
			saveFileContent = saveFileContent + saveFile.get_line()
		saveFile.close()
		globals.serverAddress = saveFileContent
	$VBoxContainer/ServerAddressContainer/HBoxContainer/TextEdit.text = globals.serverAddress


func _on_TextEdit_text_changed():
	globals.serverAddress = $VBoxContainer/ServerAddressContainer/HBoxContainer/TextEdit.text


func _on_BackButton_pressed():
	var saveFile = File.new()
	saveFile.open(globals.serverAddressFilePath, File.WRITE)
	saveFile.store_line(globals.serverAddress)
	saveFile.close()
	
	
	var error = get_tree().change_scene("res://mainmenu/MainMenu.tscn")
	if (error):
		print("Error on switch to MainMenu: " + error)
