extends MarginContainer

# Declare member variables here. Examples:
# var a = 2
var saveDir = "user://teamTournamentAnnouncer"
var lastSavePath = saveDir + "/last.save"

# Called when the node enters the scene tree for the first time.
func _ready():
	var dir = Directory.new()
	if not dir.dir_exists(saveDir):
		dir.make_dir_recursive(saveDir)
	load_save(lastSavePath)
	
	
func _on_BackButton_pressed():
	var error = get_tree().change_scene("res://mainmenu/MainMenu.tscn")
	if (error):
		print("Error on switch to MainMenu: " + error)


func _on_SaveButton_pressed():
	$SaveDialog.popup_centered()
	
	
func _on_FileDialog_file_selected(path):
	save_roles(path)
	save_roles(lastSavePath)
	_on_BackButton_pressed()


func _on_LoadButton_pressed():
	$LoadDialog.popup_centered()


func _on_LoadDialog_file_selected(path):
	load_save(path)


func load_save(path):
	var teamsSaveDict = read_teams_dict(path)
	update_role_containers(teamsSaveDict)


func save_roles(path):
	var teamsSaveDict = {
		"attack": $VBoxContainer/RoleManagementContainer/AttackManager.players,
		"defense": $VBoxContainer/RoleManagementContainer/DefenseManager.players,
		"neutral": $VBoxContainer/RoleManagementContainer/NeutralManager.players
	}
	var saveFile = File.new()
	saveFile.open(path, File.WRITE)
	var saveJson = to_json(teamsSaveDict)
	saveFile.store_line(saveJson)
	saveFile.close()
	
	
func read_teams_dict(path):
	var saveFile = File.new()
	if not saveFile.file_exists(path):
		return { "attack": [], "defense": [], "neutral": [] }
	saveFile.open(path, File.READ)
	var saveFileContent = ""
	while not saveFile.eof_reached():
		saveFileContent = saveFileContent + saveFile.get_line()
	saveFile.close()
	return parse_json(saveFileContent)
	
	
func update_role_containers(teamsSaveDict):
	$VBoxContainer/RoleManagementContainer/AttackManager.reset()
	$VBoxContainer/RoleManagementContainer/AttackManager.reload_players(teamsSaveDict["attack"])
	$VBoxContainer/RoleManagementContainer/DefenseManager.reset()
	$VBoxContainer/RoleManagementContainer/DefenseManager.reload_players(teamsSaveDict["defense"])
	$VBoxContainer/RoleManagementContainer/NeutralManager.reset()
	$VBoxContainer/RoleManagementContainer/NeutralManager.reload_players(teamsSaveDict["neutral"])
