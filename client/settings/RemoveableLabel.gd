extends HBoxContainer

signal on_remove

export(String) var text = ""

# Called when the node enters the scene tree for the first time.
func _ready():
	$Label.text = self.text

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_RemoveButton_pressed():
	emit_signal("on_remove", text)
	get_parent().remove_child(self)
	
func setText(text):
	self.text = text 