extends VBoxContainer

const RLabel = preload("res://settings/RemoveableLabel.tscn")

export(String) var roleName = "Enter title here"

var players = []

# Called when the node enters the scene tree for the first time.
func _ready():
	$RoleName.text = roleName

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_AddButton_pressed():
	var newPlayerName = $AddNameContainer/AddNameField.text
	add_label_for_new_player(newPlayerName)
	players.append(newPlayerName)
	$AddNameContainer/AddNameField.text = " "
	print(players)


func add_label_for_new_player(name):
	var newPlayerLabel = RLabel.instance()
	newPlayerLabel.setText(name)
	newPlayerLabel.connect("on_remove", self, "remove_player")
	$ScrollContainer/Players.add_child(newPlayerLabel)
	

func reload_players(players):
	reset()
	self.players = players
	for player in players:
		add_label_for_new_player(player)


func remove_player(playerLabel):
	var i = 0
	var found = false
	while i < players.size() and not found:
		if players[i] == playerLabel:
			players.remove(i)
			found = true
		i += 1
	print(players)

func reset():
	players = []
	for child in $ScrollContainer/Players.get_children():
		$ScrollContainer/Players.remove_child(child)
	
