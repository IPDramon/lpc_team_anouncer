extends KinematicBody2D

var femaleGrayHair = preload("res://assets/custom/LPC_female/hair.png")
var femaleGrayShirt = preload("res://assets/custom/LPC_female/shirt.png")
var femaleGrayPants = preload("res://assets/custom/LPC_female/pants.png")
var femaleGrayShoes = preload("res://assets/custom/LPC_female/shoes.png")
var femaleMain = preload("res://assets/custom/LPC_female/main.png")
var maleGrayHair = preload("res://assets/custom/LPC_male/gray_hair.png")
var maleGrayShirt = preload("res://assets/custom/LPC_male/gray_shirt.png")
var maleGrayPants = preload("res://assets/custom/LPC_male/gray_pants.png")
var maleGrayShoes = preload("res://assets/custom/LPC_male/gray_shoes.png")
var maleMain = preload("res://assets/custom/LPC_male/LPC-male-runcycle.png")


# class member variables go here, for example:
# var a = 2
# var b = "textvar"
export(Vector2) var moveVector = Vector2(4, 0)
var runMultiplier = 1

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	$AnimationPlayer.play("run");
#	self.recolor()

	
func _physics_process(delta):
	move_and_collide(moveVector * self.runMultiplier)
	
func set_position(newPos):
	self.position = newPos

func get_position():
	return self.position
	
func set_run_multiplier(multiplier):
	self.runMultiplier = multiplier

func recolor(spriteColoring):
	$HairSprite.modulate = random_color(spriteColoring.hair)
	$ShirtSprite.modulate = random_color(spriteColoring.shirt)
	$PantsSprite.modulate = random_color(spriteColoring.pants)
	$ShoeSprite.modulate = random_color(spriteColoring.shoes)
	
func random_color(color):
#	return Color(randf(),randf(),randf())
	return Color(color.r / 255, color.g / 255, color.b / 255)
	
func set_gender(gender):
	if gender == "MALE":
		$HairSprite.set_texture(maleGrayHair)
		$ShirtSprite.set_texture(maleGrayShirt)
		$PantsSprite.set_texture(maleGrayPants)
		$ShoeSprite.set_texture(maleGrayShoes)
		$MainSprite.set_texture(maleMain)
	else:
		$HairSprite.set_texture(femaleGrayHair)
		$ShirtSprite.set_texture(femaleGrayShirt)
		$PantsSprite.set_texture(femaleGrayPants)
		$ShoeSprite.set_texture(femaleGrayShoes)
		$MainSprite.set_texture(femaleMain)
