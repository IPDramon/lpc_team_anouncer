package de.lpc.websocket.publisher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import de.lpc.event.GameEvent;

@Component
public class WebsocketPublisher implements ApplicationListener<GameEvent> {

    @Autowired
    private SimpMessagingTemplate simpMessagengTemplate;
 
    public void processMessageFromClient(GameEvent event) {
        simpMessagengTemplate.convertAndSend("/topic/game", event.getGameDTO(), createHeaders());
    }

    @Override
    public void onApplicationEvent(GameEvent event) {
        this.processMessageFromClient(event);
    }

    private MessageHeaders createHeaders() {
        SimpMessageHeaderAccessor headerAccessor = SimpMessageHeaderAccessor.create(SimpMessageType.MESSAGE);
        headerAccessor.setLeaveMutable(true);
        return headerAccessor.getMessageHeaders();
    }
    
}