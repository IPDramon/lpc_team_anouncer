package de.lpc.service.impl;

import de.lpc.dto.PlayersByPreference;
import de.lpc.persistence.entity.Player;
import de.lpc.persistence.entity.RolePreference;
import de.lpc.persistence.entity.Tournament;
import de.lpc.persistence.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

/**
 * 
 * Player Service class
 *
 */
@Service
public class PlayerService {

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private TournamentService tournamentService;

    /**
     * @return
     */
    public List<Player> findAll() {
        return this.playerRepository.findAll();
    }

    /**
     * @param id
     * @return
     */
    public Player findById(Long id) {
        return this.playerRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(String.valueOf(id)));
    }

    /**
     * @param player
     * @return
     */
    public Player saveOrUpdate(Player player) {
        return this.playerRepository.save(player);
    }

    /**
     * @param player
     */
    public void delete(Player player) {
        this.playerRepository.delete(player);
    }

    public PlayersByPreference findPlayersByPreference(Long tournamentId) {
        Tournament tournament = tournamentService.findById(tournamentId);
        return new PlayersByPreference()
            .attackers(playerRepository.findByPreferenceForTournament(tournament, RolePreference.ATTACK))
            .defenders(playerRepository.findByPreferenceForTournament(tournament, RolePreference.DEFENSE))
            .neutrals(playerRepository.findByPreferenceForTournament(tournament, RolePreference.NONE));
    }

    public List<Player> findPlayersForPreference(Tournament tournament, RolePreference preference) {
        return playerRepository.findByPreferenceForTournament(tournament, preference);
    }

}
