package de.lpc.service.impl;

import de.lpc.persistence.entity.Tournament;
import de.lpc.persistence.repository.TournamentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

/**
 * 
 * Tournament Service class
 *
 */
@Service
public class TournamentService {

    @Autowired
    private TournamentRepository tournamentRepository;

    /**
     * @return
     */
    public List<Tournament> findAll() {
        return this.tournamentRepository.findAll();
    }

    /**
     * @param id
     * @return
     */
    public Tournament findById(Long id) {
        return this.tournamentRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(String.valueOf(id)));
    }

    /**
     * @param tournament
     * @return
     */
    public Tournament saveOrUpdate(Tournament tournament) {
        return this.tournamentRepository.save(tournament);
    }

    /**
     * @param tournament
     */
    public void delete(Tournament tournament) {
        this.tournamentRepository.delete(tournament);
    }

}
