package de.lpc.service.impl;

import org.springframework.stereotype.Service;

import de.lpc.persistence.entity.RolePreference;

import java.util.Arrays;
import java.util.List;

/**
 * 
 * Color Service class
 *
 */
@Service
public class RolePreferenceService {

    public List<RolePreference> findAll() {
        RolePreference[] options = RolePreference.values();
        return Arrays.asList(options);
    }

}
