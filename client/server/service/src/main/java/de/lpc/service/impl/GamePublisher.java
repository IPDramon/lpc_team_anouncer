package de.lpc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import de.lpc.dto.GameDTO;
import de.lpc.event.GameEvent;

/**
 * 
 * Color Service class
 *
 */
@Service
public class GamePublisher {

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    public void publishGame(GameDTO gameDTO) {
        GameEvent event = new GameEvent(this, gameDTO);
        applicationEventPublisher.publishEvent(event);
    }

}
