package de.lpc.service.impl;

import de.lpc.persistence.entity.SpriteColoring;
import de.lpc.persistence.repository.SpriteColoringRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

/**
 * 
 * SpriteColoring Service class
 *
 */
@Service
public class SpriteColoringService {

    @Autowired
    private SpriteColoringRepository spriteColoringRepository;

    /**
     * @return
     */
    public List<SpriteColoring> findAll() {
        return this.spriteColoringRepository.findAll();
    }
    
    /**
     * @param id
     * @return
     */
    public SpriteColoring findById(Long id) {
        return this.spriteColoringRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(String.valueOf(id)));
    }

    /**
     * @param spriteColoring
     * @return
     */
    public SpriteColoring saveOrUpdate(SpriteColoring spriteColoring) {
        return this.spriteColoringRepository.save(spriteColoring);
    }

    /**
     * @param spriteColoring
     */
    public void delete(SpriteColoring spriteColoring) {
        this.spriteColoringRepository.delete(spriteColoring);
    }

}
