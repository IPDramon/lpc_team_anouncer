package de.lpc.persistence.entity;

public enum RolePreference {
    ATTACK, DEFENSE, NONE
}