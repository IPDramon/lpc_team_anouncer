package de.lpc.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import de.lpc.persistence.entity.Team;

/**
 * Color Repository class
 */
@Repository
public interface TeamRepository extends JpaRepository<Team, Long> {

}
