package de.lpc.persistence.entity;

import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * Player Entity class
 */
@Entity
@Table(name = "TEAM")
public class Team extends AbstractEntity<Long> {

    //domain attributes
    @Column(name="NAME")
	private String name;
	
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="ATTACKER")
    private Player attacker;
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="DEFENDER")
    private Player defender;
    
    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="TOURNAMENT")
    private Tournament tournament;
		

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Player getAttacker() {
        return this.attacker;
    }

    public void setAttacker(Player attacker) {
        this.attacker = attacker;
    }

    public Player getDefender() {
        return this.defender;
    }

    public void setDefender(Player defender) {
        this.defender = defender;
    }

    public Tournament getTournament() {
        return this.tournament;
    }

    public void setTournament(Tournament tournament) {
        this.tournament = tournament;
    }

    public Team name(String name) {
        this.name = name;
        return this;
    }

    public Team attacker(Player attacker) {
        this.attacker = attacker;
        return this;
    }

    public Team defender(Player defender) {
        this.defender = defender;
        return this;
    }

    public Team tournament(Tournament tournament) {
        this.tournament = tournament;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Team)) {
            return false;
        }
        Team team = (Team) o;
        return Objects.equals(name, team.name) && Objects.equals(attacker, team.attacker) && Objects.equals(defender, team.defender) && Objects.equals(tournament, team.tournament);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, attacker, defender, tournament);
    }

    @Override
    public String toString() {
        return "{" +
            " name='" + getName() + "'" +
            ", attacker='" + getAttacker() + "'" +
            ", defender='" + getDefender() + "'" +
            ", tournament='" + getTournament() + "'" +
            "}";
    }
    
}
