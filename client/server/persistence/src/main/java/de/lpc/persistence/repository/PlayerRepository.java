package de.lpc.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import de.lpc.persistence.entity.Player;
import de.lpc.persistence.entity.RolePreference;
import de.lpc.persistence.entity.Tournament;

/**
 * Player Repository class
 */
@Repository
public interface PlayerRepository extends JpaRepository<Player, Long> {

    @Query("SELECT p " +
            "FROM Registration r JOIN r.player p "+
            "WHERE r.tournament = :tournament AND r.rolePreference = :preference")
    List<Player> findByPreferenceForTournament(
        @Param("tournament") Tournament tournament, 
        @Param("preference") RolePreference preference);
}
