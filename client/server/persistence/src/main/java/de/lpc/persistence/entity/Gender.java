package de.lpc.persistence.entity;

public enum Gender {
    MALE, FEMALE, DIVERSE
}