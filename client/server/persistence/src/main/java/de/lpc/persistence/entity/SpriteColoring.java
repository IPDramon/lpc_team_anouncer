package de.lpc.persistence.entity;

import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * Data Entity class
 */
@Entity
@Table(name = "SPRITE_COLORING")
public class SpriteColoring extends AbstractEntity<Long> {

    //domain attributes
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "HAIR")
    private Color hair;
    
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "SHIRT")
    private Color shirt;
    
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "PANTS")
    private Color pants;
    
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "SHOES")
    private Color shoes;


    public Color getHair() {
        return this.hair;
    }

    public void setHair(Color hair) {
        this.hair = hair;
    }

    public Color getShirt() {
        return this.shirt;
    }

    public void setShirt(Color shirt) {
        this.shirt = shirt;
    }

    public Color getPants() {
        return this.pants;
    }

    public void setPants(Color pants) {
        this.pants = pants;
    }

    public Color getShoes() {
        return this.shoes;
    }

    public void setShoes(Color shoes) {
        this.shoes = shoes;
    }

    public SpriteColoring hair(Color hair) {
        this.hair = hair;
        return this;
    }

    public SpriteColoring shirt(Color shirt) {
        this.shirt = shirt;
        return this;
    }

    public SpriteColoring pants(Color pants) {
        this.pants = pants;
        return this;
    }

    public SpriteColoring shoes(Color shoes) {
        this.shoes = shoes;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof SpriteColoring)) {
            return false;
        }
        SpriteColoring spriteColoring = (SpriteColoring) o;
        return Objects.equals(hair, spriteColoring.hair) && Objects.equals(shirt, spriteColoring.shirt) && Objects.equals(pants, spriteColoring.pants) && Objects.equals(shoes, spriteColoring.shoes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hair, shirt, pants, shoes);
    }

    @Override
    public String toString() {
        return "{" +
            " hair='" + getHair() + "'" +
            ", shirt='" + getShirt() + "'" +
            ", pants='" + getPants() + "'" +
            ", shoes='" + getShoes() + "'" +
            "}";
    }

}
