package de.lpc.persistence.repository;

import de.lpc.persistence.entity.Color;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Color Repository class
 */
@Repository
public interface ColorRepository extends JpaRepository<Color, Long> {

}
