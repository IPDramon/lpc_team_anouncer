package de.lpc.persistence.entity;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * Color Entity class
 */
@Entity
@Table(name = "COLOR")
public class Color extends AbstractEntity<Long> {

    //domain attributes
    @Column(name="R")
	private Double r;
	
    @Column(name="G")
	private Double g;
	
    @Column(name="B")
	private Double b;

    public Double getR() {
        return this.r;
    }

    public void setR(Double r) {
        this.r = r;
    }

    public Double getG() {
        return this.g;
    }

    public void setG(Double g) {
        this.g = g;
    }

    public Double getB() {
        return this.b;
    }

    public void setB(Double b) {
        this.b = b;
    }

    public Color r(Double r) {
        this.r = r;
        return this;
    }

    public Color g(Double g) {
        this.g = g;
        return this;
    }

    public Color b(Double b) {
        this.b = b;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Color)) {
            return false;
        }
        Color color = (Color) o;
        return Objects.equals(r, color.r) && Objects.equals(g, color.g) && Objects.equals(b, color.b);
    }

    @Override
    public int hashCode() {
        return Objects.hash(r, g, b);
    }

    @Override
    public String toString() {
        return "{" +
            " r='" + getR() + "'" +
            ", g='" + getG() + "'" +
            ", b='" + getB() + "'" +
            "}";
    }

}
