package de.lpc.dto;

import java.util.List;
import java.util.Objects;
import java.util.ArrayList;
import de.lpc.persistence.entity.Player;

public class GameDTO {

    private List<Player> players = new ArrayList<>();

    public GameDTO() {
    }

    public GameDTO(List<Player> players) {
        this.players = players;
    }

    public List<de.lpc.persistence.entity.Player> getPlayers() {
        return this.players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public GameDTO players(List<Player> players) {
        this.players = players;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof GameDTO)) {
            return false;
        }
        GameDTO gameDTO = (GameDTO) o;
        return Objects.equals(players, gameDTO.players);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(players);
    }

    @Override
    public String toString() {
        return "{" +
            " players='" + getPlayers() + "'" +
            "}";
    }


}