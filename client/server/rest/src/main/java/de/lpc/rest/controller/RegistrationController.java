package de.lpc.rest.controller;

import de.lpc.persistence.entity.Registration;
import de.lpc.service.impl.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Registration RestController
 * Have a look at the RequestMapping!!!!!!
 */
@RestController
@RequestMapping("${rest.base-path}/registrations")
public class RegistrationController {

    @Autowired
    private RegistrationService registrationService;

    @GetMapping
    public List<Registration> findAll() {
        return this.registrationService.findAll();
    }

    @GetMapping(value = "/{id}")
    public Registration findById(@PathVariable("id") Long id) {
        return this.registrationService.findById(id);
    }

    /**
     * @param registration
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Registration save(@RequestBody Registration registration) {
        return this.registrationService.save(registration);
    }

    @PutMapping
    @ResponseStatus(value = HttpStatus.OK)
    public Registration update(@RequestBody Registration registration) {
        return this.registrationService.update(registration);
    }

    @DeleteMapping
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@RequestBody Registration registration) {
        this.registrationService.delete(registration);
    }

}
