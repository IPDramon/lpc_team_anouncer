package de.lpc.rest.controller;

import de.lpc.dto.GameDTO;
import de.lpc.service.impl.GamePublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * Player RestController
 * Have a look at the RequestMapping!!!!!!
 */
@RestController
@RequestMapping("${rest.base-path}/games")
public class GameController {

    @Autowired
    private GamePublisher gamePublisher;

    @PutMapping
    @ResponseStatus(value = HttpStatus.OK)
    public Boolean update(@RequestBody GameDTO gameDTO) {
        this.gamePublisher.publishGame(gameDTO);
        return true;
    }

}
