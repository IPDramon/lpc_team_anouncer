package de.lpc.rest.controller;

import de.lpc.persistence.entity.Tournament;
import de.lpc.service.impl.TournamentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Tournament RestController
 * Have a look at the RequestMapping!!!!!!
 */
@RestController
@RequestMapping("${rest.base-path}/tournaments")
public class TournamentController {

    @Autowired
    private TournamentService tournamentService;

    @GetMapping
    public List<Tournament> findAll() {
        return this.tournamentService.findAll();
    }

    @GetMapping(value = "/{id}")
    public Tournament findById(@PathVariable("id") Long id) {
        return this.tournamentService.findById(id);
    }

    /**
     * @param tournament
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Tournament save(@RequestBody Tournament tournament) {
        return this.tournamentService.saveOrUpdate(tournament);
    }

    @PutMapping
    @ResponseStatus(value = HttpStatus.OK)
    public Tournament update(@RequestBody Tournament tournament) {
        return this.tournamentService.saveOrUpdate(tournament);
    }

    @DeleteMapping
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@RequestBody Tournament tournament) {
        this.tournamentService.delete(tournament);
    }

}
