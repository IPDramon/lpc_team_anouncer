import CrudRest from "./CrudRest";

class RolePreferenceRest extends CrudRest {

    constructor() {
        super(window.location.pathname + "api/rolePreferences");
    }

}

export default RolePreferenceRest;
