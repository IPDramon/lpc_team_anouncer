import CrudRest from "./CrudRest";

class PlayerRest extends CrudRest {

    constructor() {
        super(window.location.pathname + "api/tournaments");
    }

}

export default PlayerRest;
