import React, { Component } from 'react';
import { withTranslation } from "react-i18next";
import Typography from "@material-ui/core/Typography";

import AllTable from '../../../commons/table/AllTable';
import { compose } from 'recompose';

import RegistrationRest from '../../../services/RegistrationRest';
import { Button } from '@material-ui/core';
import { withRouter } from 'react-router-dom';

class RegistrationAll extends Component {

    state = {
        registrations: [],
        selected: undefined,
    }

    columns = [];

    registrationRest = new RegistrationRest();

    componentDidMount() {

        const t = this.props.t;

        this.columns = [
            { title: t('registration.id'), field: 'id' },
            { title: t('tournament.title'), field: 'tournament.name' },
            { title: t('player.title'), field: 'player.name' },
            { title: t('rolePreference.title'), field: 'rolePreference' }
        ];

        this.fetchAll();
    }

    fetchAll = () => {
        this.registrationRest.findAll().then(
            response => {
                this.setState({registrations: response.data})
            }, err => console.error(err));
    }

    render() {

        const { registrations, selected } = this.state;
        const { t, history } = this.props;

        return (
            <React.Fragment>
                <Typography variant="h1" color="primary">{t('registration.title')}</Typography>
                <Button onClick={() => history.push("/registrations/create")}>{t('button.create')}</Button>
                <Button onClick={this.goToUpdate}>{t('button.update')}</Button>
                <Button onClick={() => this.registrationRest.delete(selected).then(this.fetchAll)}>{t('button.delete')}</Button>
                <AllTable
                    entities={registrations}
                    columns={this.columns}
                    selected={selected}
                    onRowClick={this.onRowClick} />
                
            </React.Fragment>
        );
    }

    goToUpdate = () => {
        const { selected } = this.state;
        const { history } = this.props;
        if (this.state.selected) {
            history.push("/registrations/update/" + selected.id);
        }
    }

    onRowClick = (registration) => {
        this.setState({selected: registration});
    }

}

export default compose(withTranslation())(withRouter(RegistrationAll));
