import React, { Component } from 'react';
import {Route} from 'react-router-dom';
import RegistrationAll from "./all/RegistrationAll";
import RegistrationSingle from './single/RegistrationSingle';

class RegistrationMain extends Component {

  render() {

    return (
        <React.Fragment>
          <Route exact path="/registrations" component={RegistrationAll}/>
          <Route exact path="/registrations/create" component={RegistrationSingle}/>
          <Route exact path="/registrations/update/:id" component={RegistrationSingle}/>
        </React.Fragment>
    );
  }
}

export default RegistrationMain;
