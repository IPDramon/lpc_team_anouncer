import React, { Component } from 'react';
import { withRouter } from "react-router";
import { withTranslation } from "react-i18next";
import Typography from "@material-ui/core/Typography";

import AddUpdateForm from '../../../commons/form/AddUpdateForm';
import { compose } from 'recompose';

import RegistrationRest from '../../../services/RegistrationRest';
import PlayerRest from '../../../services/PlayerRest';
import TournamentRest from '../../../services/TournamentRest';
import Axios from 'axios';
import RolePreferenceRest from '../../../services/RolePreferenceRest';

class RegistrationSingle extends Component {

    state = {
        titleKey: "",
        registration: {
            id: null,
            player: null,
            rolePreference: null,
            tournament: null
        },
        attributes: [
            {name: "tournament", type: "select", items: [], render: t => t.name },
            {name: "player", type: "select", items: [], render: p => p.name},
            {name: "rolePreference", type: "select", items: [], render: r => r},
        ],
        isLoading: true
    }

    registrationRest = new RegistrationRest();
    playerRest = new PlayerRest();
    rolePreferenceRest = new RolePreferenceRest();
    tournamentRest = new TournamentRest();

    componentDidMount() {
        let updateResult = this.checkUpdate();
        this.submitFunction = updateResult.submitFunction;
        this.setState({ titleKey: updateResult.titleKey });

        let promises = [];
        promises.push(this.tournamentRest.findAll());
        promises.push(this.playerRest.findAll());
        promises.push(this.rolePreferenceRest.findAll());
        if (updateResult.updatePromise) {
            console.debug("Called update promise");
            promises.push(updateResult.updatePromise);
        }

        Axios.all(promises).then(responses => {
            let result = {isLoading: false};

            const tournaments = responses[0].data;
            const players = responses[1].data;
            const rolePreferences = responses[2].data;

            let { attributes } = this.state;
            
            attributes[0].items = tournaments;
            attributes[1].items = players;
            attributes[2].items = rolePreferences;

            if (updateResult.updatePromise) {
                let data = responses[3].data;
                let registration = data;

                registration.player = players[players.findIndex(player => player.id === data.player.id)];
                registration.tournament = tournaments[tournaments.findIndex(tournament => tournament.id === data.tournament.id)];
                registration.rolePreference = rolePreferences[rolePreferences.findIndex(rp => rp === data.rolePreference)];

                result.registration = registration;
                
            }

            result.attributes = attributes;
            
            this.setState(result);

        });
    }

    render() {

        const { registration, isLoading, attributes } = this.state;
        const { t } = this.props;

        return (
            <React.Fragment>
                <Typography variant="h1" color="primary">{t('registration.title')}</Typography>

                {!isLoading &&
                    <React.Fragment>
                        <AddUpdateForm
                            entity={registration}
                            prefix="registration"
                            attributes={attributes}
                            handleChange={this.handleChange}
                            handleSubmit={this.handleSubmit} 
                        />
                    </React.Fragment>
                }
            </React.Fragment>
        );
    }

    checkUpdate() {
        let func;
        let titleKey = "";
        let updatePromise = null;
        if (this.props.match.params.id === undefined) {
            func = this.registrationRest.create;
            titleKey = "form.create";
        } else {
            updatePromise = this.registrationRest.findById(this.props.match.params.id);
            func = this.registrationRest.update;
            titleKey = "form.update";
        }
        return { submitFunction: func, titleKey: titleKey, updatePromise };
    };

    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        let registration = this.state.registration;
        registration[name] = value;

        this.setState({
            registration: registration
        });
    };

    updatePlayer = (registration) => {
        this.setState({ registration });
    }

    handleSubmit = (event) => {

        //turn off page relaod
        event.preventDefault();

        let registration = this.state.registration;

        this.submitFunction(registration).then(this.goBack);
    };

    goBack = () => {
        this.props.history.push("/registrations");
    };

}

export default compose(withTranslation())(withRouter(RegistrationSingle));
