import React, { Component } from 'react';

import femaleHair from '../../../assets/LPC_female/hair.png';
import femaleShirt from '../../../assets/LPC_female/shirt.png';
import femalePants from '../../../assets/LPC_female/pants.png';
import femaleShoes from '../../../assets/LPC_female/shoes.png';
import femaleMain from '../../../assets/LPC_female/main.png';

import maleHair from '../../../assets/LPC_male/hair.png';
import maleShirt from '../../../assets/LPC_male/shirt.png';
import malePants from '../../../assets/LPC_male/pants.png';
import maleShoes from '../../../assets/LPC_male/shoes.png';
import maleMain from '../../../assets/LPC_male/main.png';

import "./SpriteColorer.css";
import HueChanger from './HueChanger';

class SpriteColorer extends Component {

  state = {
    currentFrame: 0
  }

  parts = [
    "hair",
    "shirt",
    "pants",
    "shoes"
  ];

  componentDidMount = () => {
    setInterval(() => {
      const currentFrame = this.state.currentFrame;
      this.setState({ currentFrame: (currentFrame + 1) % 8 });
    }, 100)
  }

  render() {

    const { currentFrame } = this.state;
    const { player } = this.props;
    let sprite = <div>NO SPRITE</div>;
    if ( player.gender === "FEMALE" ) {
      sprite = (
        <g>
          <image width="512" height="64" className="pixelated" transform="scale(5,5)" xlinkHref={femaleMain} alt="logo" />
          <image width="512" height="64" className="pixelated" transform="scale(5,5)" filter="url(#hairFilter)" xlinkHref={femaleHair} alt="logo" />
          <image width="512" height="64" className="pixelated" transform="scale(5,5)" filter="url(#shirtFilter)" xlinkHref={femaleShirt} alt="logo" />
          <image width="512" height="64" className="pixelated" transform="scale(5,5)" filter="url(#pantsFilter)" xlinkHref={femalePants} alt="logo" />
          <image width="512" height="64" className="pixelated" transform="scale(5,5)" filter="url(#shoesFilter)" xlinkHref={femaleShoes} alt="logo" />
        </g>
      );
    } else {
      sprite = (
        <g>
          <image width="512" height="64" className="pixelated" transform="scale(5,5)" xlinkHref={maleMain} alt="logo" />
          <image width="512" height="64" className="pixelated" transform="scale(5,5)" filter="url(#hairFilter)" xlinkHref={maleHair} alt="logo" />
          <image width="512" height="64" className="pixelated" transform="scale(5,5)" filter="url(#shirtFilter)" xlinkHref={maleShirt} alt="logo" />
          <image width="512" height="64" className="pixelated" transform="scale(5,5)" filter="url(#pantsFilter)" xlinkHref={malePants} alt="logo" />
          <image width="512" height="64" className="pixelated" transform="scale(5,5)" filter="url(#shoesFilter)" xlinkHref={maleShoes} alt="logo" />
        </g>
      );
    }

    return (
      <React.Fragment>
        <svg width="320" height="320" viewBox={currentFrame * 320 + " 0 320 320"}>
          {this.parts.map(part => this.createFilterForColor(player.spriteColoring[part], part))}
          {sprite}
        </svg>
        <div className="flex-container">
          <div>
            <form onChange={this.changeGender}>
              <input type="radio" id="MALE" name="gender" value="MALE" checked={player.gender === "MALE"} onChange={() => console.info("some changes...")}/>
              <label htmlFor="MALE">Sprite#1</label><br/>
              <input type="radio" id="FEMALE" name="gender" value="FEMALE" checked={player.gender === "FEMALE"} onChange={() => console.info("some changes...")}/>
              <label htmlFor="FEMALE">Sprite#2</label>
            </form>
          </div>
          {this.parts.map(part => <HueChanger key={ part } color={player.spriteColoring[part]} setColorGenerator={this.setColorGenerator} partName={part} />)}
        </div>
      </React.Fragment>
      );
  }

  createFilterForColor = (color, part) => {
    return (
      <filter key={ part } id={part + "Filter"}>
        <feColorMatrix type="matrix" values={color.r / 255.0 + " 0 0 0 0, 0 " + color.g / 255.0 + " 0 0 0, 0 0 " + color.b / 255.0 + " 0 0, 0 0 0 1 0"} />
      </filter>);
  }

  changeGender = (event) => {
    let player = this.props.player;
    player.gender = event.target.value;
    this.props.updatePlayer(player);
  }

  setColorGenerator = (partName) => {
    return (color) => {
      let player = this.props.player;
      player.spriteColoring[partName] = color.rgb;
      this.props.updatePlayer(player);
    }
  }


}

export default SpriteColorer;
