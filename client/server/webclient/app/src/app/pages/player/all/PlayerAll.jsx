import React, { Component } from 'react';
import { withTranslation } from "react-i18next";
import Typography from "@material-ui/core/Typography";

import AllTable from '../../../commons/table/AllTable';
import { compose } from 'recompose';

import PlayerRest from '../../../services/PlayerRest';
import { Button } from '@material-ui/core';
import { withRouter } from 'react-router-dom';

class PlayerAll extends Component {

    state = {
        players: [],
        selected: undefined,
    }

    columns = [];

    playerRest = new PlayerRest();

    componentDidMount() {

        const t = this.props.t;

        this.columns = [
            { title: t('player.id'), field: 'id' },
            { title: t('player.name'), field: 'name' }
        ];

        this.fetchAll();
    }

    fetchAll = () => {
        this.playerRest.findAll().then(
            response => {
                this.setState({players: response.data})
            }, err => console.error(err));
    }

    render() {

        const { players, selected } = this.state;
        const { t, history } = this.props;

        return (
            <React.Fragment>
                <Typography variant="h1" color="primary">{t('player.title')}</Typography>
                <Button onClick={() => history.push("/players/create")}>{t('button.create')}</Button>
                <Button onClick={this.goToUpdate}>{t('button.update')}</Button>
                <Button onClick={() => this.playerRest.delete(selected).then(this.fetchAll)}>{t('button.delete')}</Button>
                <AllTable
                    entities={players}
                    columns={this.columns}
                    selected={selected}
                    onRowClick={this.onRowClick} />
                
            </React.Fragment>
        );
    }

    goToUpdate = () => {
        const { selected } = this.state;
        const { history } = this.props;
        if (this.state.selected) {
            history.push("/players/update/" + selected.id);
        }
    }

    onRowClick = (player) => {
        this.setState({selected: player});
    }

}

export default compose(withTranslation())(withRouter(PlayerAll));
