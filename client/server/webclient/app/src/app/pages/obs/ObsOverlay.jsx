import React, { Component } from "react";
import "./ObsOverlay.css";
import SockJS from "sockjs-client";
import Stomp from "webstomp-client";

class ObsOverlay extends Component {

    state = {
        teams: [
            {
                attack: 'A1',
                defense: 'D1'
            },
            {
                attack: 'A2',
                defense: 'D2'
            },
        ]
    }

    stompClient = {};

    componentDidMount() {
        this.connect();
    }

    connect = () => {
        let url = window.location.pathname + 'websocket';
        let sock = new SockJS(url);
        let stomp = Stomp.over(sock);

        this.stompClient = stomp;
        this.stompClient.connect({}, this.onConnected, this.onError);
        this.stompClient.reconnect_delay = 3000;
    }

    onConnected = (data) => {
        this.stompClient.subscribe('/topic/game', this.onMessage);
    }

    onMessage = (payload) => {
        let players = JSON.parse(payload.body).players;

        let teams = [];
        let team1 = {};
        let team2 = {};
        team1.attack = players[0];
        team1.defense = players[1];
        team2.attack = players[2];
        team2.defense = players[3];

        teams.push(team1);
        teams.push(team2);

        this.setState({teams: teams});
    }

    onError = (errorData) => {
        console.error(errorData);
    }

    render() {

        const {teams} = this.state;

        return (
            <div className="main">
                <div className="teamField top">{teams[0].attack.name + " & " + teams[0].defense.name}</div>
                <div className="teamField bottom">{teams[1].attack.name + " & " + teams[1].defense.name}</div>
            </div>
        );
    }

    componentWillUnmount() {
        this.stompClient.disconnect();
    }

}

export default ObsOverlay;