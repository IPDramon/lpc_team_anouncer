import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import { withRouter } from "react-router-dom";
// Material UI Components
import { withStyles } from "@material-ui/core/styles";
// Page Components
import Home from "./home/HomeMain";

import MenuStyles from "../assets/styles/MenuStyles";
import PlayerMain from "./player/PlayerMain";
import TournamentMain from "./tournament/TournamentMain";
import RegistrationMain from "./registration/RegistrationMain";
import ObsOverlay from "./obs/ObsOverlay";

class MainContentRouter extends Component {

    render() {
        const { classes, location } = this.props;

        return (
            <main className={location.pathname.endsWith("obs") ? null : classes.content}>
                <div className={location.pathname.endsWith("obs") ? null : classes.toolbar} />
                <Switch>
                    <Route
                        path="/players"
                        component={PlayerMain}/>
                    <Route
                        path="/tournaments"
                        component={TournamentMain}/>
                    <Route
                        path="/registrations"
                        component={RegistrationMain}/>
                    <Route
                        path="/obs"
                        component={ObsOverlay}/>
                    <Route
                        path="/"
                        component={Home}/>
                </Switch>
            </main>
        );
    }
}

export default withStyles(MenuStyles, { withTheme: true })(withRouter(MainContentRouter));
