import React, { Component } from 'react';
import {Route} from 'react-router-dom';
import TournamentAll from "./all/TournamentAll";
import TournamentSingle from './single/TournamentSingle';

class TournamentMain extends Component {

  render() {

    return (
        <React.Fragment>
          <Route exact path="/tournaments" component={TournamentAll}/>
          <Route exact path="/tournaments/create" component={TournamentSingle}/>
          <Route exact path="/tournaments/update/:id" component={TournamentSingle}/>
        </React.Fragment>
    );
  }
}

export default TournamentMain;
