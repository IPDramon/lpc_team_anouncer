import React, { Component } from 'react';
import { withTranslation } from "react-i18next";
import Typography from "@material-ui/core/Typography";

import AllTable from '../../../commons/table/AllTable';
import { compose } from 'recompose';

import TournamentRest from '../../../services/TournamentRest';
import { Button } from '@material-ui/core';
import { withRouter } from 'react-router-dom';

class TournamentAll extends Component {

    state = {
        tournaments: [],
        selected: undefined,
    }

    columns = [];

    tournamentRest = new TournamentRest();

    componentDidMount() {

        const t = this.props.t;

        this.columns = [
            { title: t('tournament.id'), field: 'id' },
            { title: t('tournament.name'), field: 'name' }
        ];

        this.fetchAll();
    }

    fetchAll = () => {
        this.tournamentRest.findAll().then(
            response => {
                this.setState({tournaments: response.data})
            }, err => console.error(err));
    }

    render() {

        const { tournaments, selected } = this.state;
        const { t, history } = this.props;

        return (
            <React.Fragment>
                <Typography variant="h1" color="primary">{t('tournament.title')}</Typography>
                <Button onClick={() => history.push("/tournaments/create")}>{t('button.create')}</Button>
                <Button onClick={this.goToUpdate}>{t('button.update')}</Button>
                <Button onClick={() => this.tournamentRest.delete(selected).then(this.fetchAll)}>{t('button.delete')}</Button>
                <AllTable
                    entities={tournaments}
                    columns={this.columns}
                    selected={selected}
                    onRowClick={this.onRowClick} />
                
            </React.Fragment>
        );
    }

    goToUpdate = () => {
        const { selected } = this.state;
        const { history } = this.props;
        if (this.state.selected) {
            history.push("/tournaments/update/" + selected.id);
        }
    }

    onRowClick = (tournament) => {
        this.setState({selected: tournament});
    }

}

export default compose(withTranslation())(withRouter(TournamentAll));
