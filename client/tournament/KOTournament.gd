extends Node2D

const TournamentField = preload("res://tournament/TournamentField.tscn")
const BasicLine = preload("res://tournament/BasicLine.tscn")

export(float) var fieldOffsetFactor = 1.33
export(float) var scrollSpeed = 750

var windowSize
var koSystemWidth
var scrollingOn = false

# Called when the node enters the scene tree for the first time.
func _ready():
	
	windowSize = Vector2(ProjectSettings.get_setting("display/window/size/width"), ProjectSettings.get_setting("display/window/size/height"))
		
	var fieldSize = TournamentField.instance().get_size()

	var teams = globals.teams
	
	var maxLevel = calc_ko_max_level()

	var fields = add_first_level(maxLevel, windowSize, teams)

	for i in range(maxLevel):
		fields = add_level(fields, i)
		if (i+1 == maxLevel):
			fields[0].connect("winner_found", self, "finish_tournament")
		
	koSystemWidth = maxLevel * fieldSize.x * fieldOffsetFactor + fieldSize.x
	var koSystemHeight = pow(2, maxLevel) * fieldSize.y + 100
	
	var maxXScale = 1.0 * windowSize.x / koSystemWidth
	var maxYScale = 1.0 * windowSize.y / koSystemHeight
	scrollingOn = maxXScale < maxYScale
	
#	print(fieldSize.x)
#	print(fieldSize.y)
#	print("---")
#	print(koSystemWidth)
#	print(koSystemHeight)
#	print("---")
#	print(maxXScale)
#	print(maxYScale)
	
	var calculatedScale = min(maxXScale, maxYScale)
#	var calculatedScale = maxYScale
	
	$FieldContainer.scale = Vector2(calculatedScale, calculatedScale)
	$ScrollBar.scale.x = windowSize.x / 3
	if not scrollingOn:
		$ScrollBar.visible = false
	
	

func add_first_level(maxLevel, windowSize, teams):
	var numElements = int(pow(2, maxLevel))
	var fieldSize = TournamentField.instance().get_size()
	var yOffset = fieldSize.y / 2
	var baseOffset = yOffset
	var fields = []
	var team2Index = numElements
	for i in range(numElements):
		if i >= numElements / 2 and team2Index % 2 == 0:
			team2Index = numElements + 1
		var field = TournamentField.instance()
		var position = Vector2(0, i * fieldSize.y + yOffset)
		field.set_position(position)
		field.set_team_1(teams[i])
		if team2Index < teams.size():
			field.set_team_2(teams[team2Index])
		team2Index = team2Index + 2
		fields.append(field)
		$FieldContainer.add_child(field)
	
	return fields
	
func add_level(fields, level):
	var numElements = fields.size()
	var newFields = []
	for i in range(0, numElements, 2):
		var up = i
		var down = i + 1
		var field = TournamentField.instance()
		var upField = fields[up]
		var downField = fields[down]
		var position = Vector2(field.get_size().x * fieldOffsetFactor * (level + 1), (upField.get_position().y + downField.get_position().y) / 2)
		field.set_position(position)
		newFields.append(field)
		$FieldContainer.add_child(field)
		
		#add basic line
		var lineWidth = (fieldOffsetFactor - 1) / 2 * field.get_size().x
		var upUpline = BasicLine.instance()
		upUpline.scale = Vector2(lineWidth, 1)
		upUpline.position = Vector2(upField.get_size().x, 0)
		upField.add_child(upUpline)
		
		var upVertLine = BasicLine.instance()
		upVertLine.scale = Vector2(1, upField.get_position().y - field.get_position().y)
		upVertLine.position = Vector2(upField.get_size().x + lineWidth, -upVertLine.scale.y / 2)
		upField.add_child(upVertLine)
		
		var upDownLine = BasicLine.instance()
		upDownLine.scale = upUpline.scale
		upDownLine.position = Vector2(upField.get_size().x + lineWidth, (downField.get_position().y - upField.get_position().y) / 2)
		upField.add_child(upDownLine)
		
		var downVertLine = BasicLine.instance()
		downVertLine.scale = upVertLine.scale
		downVertLine.position = Vector2(upField.get_size().x + lineWidth, upVertLine.scale.y / 2)
		downField.add_child(downVertLine)
		
		var downDownLine = BasicLine.instance()
		downDownLine.scale = upUpline.scale
		downDownLine.position = Vector2(downField.get_size().x, 0)
		downField.add_child(downDownLine)
		
		#connect signals
		upField.connect("winner_found", field, "set_team_1")
		downField.connect("winner_found", field, "set_team_2")
		
	return newFields
	
func calc_ko_max_level():
	var i = 0
	while globals.teams.size() > pow(2, i):
		i += 1
	return i - 1
	
func _process(delta):
	if scrollingOn:
		var maxScrollbarScroll = windowSize.x - $ScrollBar.scale.x
		var maxFieldScroll = abs(koSystemWidth - windowSize.x)
		var change = scrollSpeed * delta
		if Input.is_action_pressed("ui_right"):
			var newScrollX = $ScrollBar.position.x + change
			$ScrollBar.position.x = min(newScrollX, maxScrollbarScroll)
			
			var newFieldX = $FieldContainer.position.x - change / maxScrollbarScroll * maxFieldScroll
			print(newFieldX)
			$FieldContainer.position.x = max(newFieldX, -maxFieldScroll)
			
		elif Input.is_action_pressed("ui_left"):
			var newScrollX = $ScrollBar.position.x - change
			$ScrollBar.position.x = max(newScrollX, 0)
			
			var newFieldX = $FieldContainer.position.x + change / maxScrollbarScroll * maxFieldScroll
			print(newFieldX)
			$FieldContainer.position.x = min(newFieldX, 0)

func finish_tournament(winner):
	globals.tournamentWinner = winner
	var error = get_tree().change_scene("res://tournament/WinnerAnnouncement.tscn")
	if (error):
		print("Error on switch to Start: " + error)