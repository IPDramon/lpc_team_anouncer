extends Node2D

const TournamentField = preload("res://tournament/TournamentField.tscn")
const BasicLine = preload("res://tournament/BasicLine.tscn")

export(float) var fieldOffsetFactor = 1.33
export(float) var scrollSpeed = 750

var windowSize
var koSystemWidth
var scrollingOn = true
var boundsX

var leftSideFinished = false
var rightSideFinished = false

# Called when the node enters the scene tree for the first time.
func _ready():
	
	windowSize = Vector2(ProjectSettings.get_setting("display/window/size/width"), ProjectSettings.get_setting("display/window/size/height"))
		
	var fieldSize = TournamentField.instance().get_size()

	var teams = globals.teams
	
	var maxLevel = calc_ko_max_level()

	var fields = add_first_level(maxLevel, windowSize, teams)
	
	var rightFields = add_winner_only_level(fields, 0)
	var leftFields = add_loser_only_level(fields, "loser_found")

	for i in range(1, maxLevel):
		leftFields = add_mixed_levels(rightFields, leftFields)
		rightFields = add_winner_only_level(rightFields, i)
	
	boundsX = add_final_levels(rightFields, leftFields)
	
	koSystemWidth = abs(boundsX.x) + abs(boundsX.y)
	var koSystemHeight = pow(2, maxLevel) * fieldSize.y + 100 
	
	var maxXScale = 1.0 * windowSize.x / koSystemWidth
	var maxYScale = 1.0 * windowSize.y / koSystemHeight
	scrollingOn = maxXScale < maxYScale
	
#	var calculatedScale = min(maxXScale, maxYScale)
	var calculatedScale = maxYScale
	
	boundsX *= calculatedScale
	
	$FieldContainer.scale = Vector2(calculatedScale, calculatedScale)
	$ScrollBar.scale.x = windowSize.x / 3
	$ScrollBar/Camera2D.limit_left = boundsX.x
	$ScrollBar/Camera2D.limit_right = boundsX.y
	

func add_first_level(maxLevel, windowSize, teams):
	var numElements = int(pow(2, maxLevel))
	var fieldSize = TournamentField.instance().get_size()
	var yOffset = fieldSize.y / 2
	var baseOffset = yOffset
	var fields = []
	var team2Index = numElements
	for i in range(numElements):
		if i >= numElements / 2 and team2Index % 2 == 0:
			team2Index = numElements + 1
		var field = TournamentField.instance()
		var position = Vector2(0, i * fieldSize.y + yOffset)
		field.set_position(position)
		field.set_team_1(teams[i])
		if team2Index < teams.size():
			field.set_team_2(teams[team2Index])
		team2Index = team2Index + 2
		fields.append(field)
		$FieldContainer.add_child(field)
	
	return fields
	
func add_winner_only_level(fields, level):
	var numElements = fields.size()
	var newFields = []
	for i in range(0, numElements, 2):
		var up = i
		var down = i + 1
		var field = TournamentField.instance()
		var upField = fields[up]
		var downField = fields[down]
		var position = Vector2(field.get_size().x * fieldOffsetFactor * (level + 1), (upField.get_position().y + downField.get_position().y) / 2)
		field.set_position(position)
		newFields.append(field)
		$FieldContainer.add_child(field)
		
		#add basic line
		var lineWidth = (fieldOffsetFactor - 1) / 2 * field.get_size().x
		var upUpline = BasicLine.instance()
		upUpline.scale = Vector2(lineWidth, 1)
		upUpline.position = Vector2(upField.get_size().x, 0)
		upField.add_child(upUpline)
		
		var upVertLine = BasicLine.instance()
		upVertLine.scale = Vector2(1, upField.get_position().y - field.get_position().y)
		upVertLine.position = Vector2(upField.get_size().x + lineWidth, -upVertLine.scale.y / 2)
		upField.add_child(upVertLine)
		
		var upDownLine = BasicLine.instance()
		upDownLine.scale = upUpline.scale
		upDownLine.position = Vector2(upField.get_size().x + lineWidth, (downField.get_position().y - upField.get_position().y) / 2)
		upField.add_child(upDownLine)
		
		var downVertLine = BasicLine.instance()
		downVertLine.scale = upVertLine.scale
		downVertLine.position = Vector2(upField.get_size().x + lineWidth, upVertLine.scale.y / 2)
		downField.add_child(downVertLine)
		
		var downDownLine = BasicLine.instance()
		downDownLine.scale = upUpline.scale
		downDownLine.position = Vector2(downField.get_size().x, 0)
		downField.add_child(downDownLine)
		
		#connect signals
		upField.connect("winner_found", field, "set_team_1")
		downField.connect("winner_found", field, "set_team_2")
		
	return newFields
	
	
func add_loser_only_level(fields, signalName):
	var numElements = fields.size()
	var newFields = []
	for i in range(0, numElements, 2):
		var up = i
		var down = i + 1
		var field = TournamentField.instance()
		var upField = fields[up]
		var downField = fields[down]
		var position = Vector2(-1 * field.get_size().x * fieldOffsetFactor + upField.get_position().x, (upField.get_position().y + downField.get_position().y) / 2)
		field.set_position(position)
		newFields.append(field)
		$FieldContainer.add_child(field)
		
		var lineWidth = (fieldOffsetFactor - 1) / 2 * field.get_size().x
		var upUpline = BasicLine.instance()
		upUpline.scale = Vector2(-lineWidth, 1)
		upUpline.position = Vector2(0, 0)
		upField.add_child(upUpline)
		
		var upVertLine = BasicLine.instance()
		upVertLine.scale = Vector2(1, upField.get_position().y - field.get_position().y)
		upVertLine.position = Vector2(-lineWidth, -upVertLine.scale.y / 2)
		upField.add_child(upVertLine)
		
		var upDownLine = BasicLine.instance()
		upDownLine.scale = upUpline.scale
		upDownLine.position = Vector2(-lineWidth, (downField.get_position().y - upField.get_position().y) / 2)
		upField.add_child(upDownLine)
		
		var downVertLine = BasicLine.instance()
		downVertLine.scale = upVertLine.scale
		downVertLine.position = Vector2(-lineWidth, upVertLine.scale.y / 2)
		downField.add_child(downVertLine)
		
		var downDownLine = BasicLine.instance()
		downDownLine.scale = upUpline.scale
		downDownLine.position = Vector2(0, 0)
		downField.add_child(downDownLine)
		
		#connect signals
		upField.connect(signalName, field, "set_team_1")
		downField.connect(signalName, field, "set_team_2")
	return newFields
		
		
func add_mixed_levels(rightFields, leftFields):
	var numElements = leftFields.size()
	var newFields = []
	for i in range(numElements):
		var field = TournamentField.instance()
		var losersField = rightFields[rightFields.size() - i - 1]
		var winnersField = leftFields[i]
		var xSize = field.get_size().x
		var position = Vector2(-1 * field.get_size().x * fieldOffsetFactor + winnersField.get_position().x, winnersField.get_position().y)
		field.set_position(position)
		newFields.append(field)
		$FieldContainer.add_child(field)
		
		var lineWidth = (fieldOffsetFactor - 1) * field.get_size().x
		var line = BasicLine.instance()
		line.scale = Vector2(-lineWidth, 1)
		line.position = Vector2(0, 0)
		winnersField.add_child(line)
		
		winnersField.connect("winner_found", field, "set_team_1")
		losersField.connect("loser_found", field, "set_team_2")
		
	return add_loser_only_level(newFields, "winner_found")


func add_final_levels(rightFields, leftFields):
	var field = TournamentField.instance()
	var leftmostField = leftFields[0]
	var rightmostField = rightFields[0]
	var position = Vector2(-1 * field.get_size().x * fieldOffsetFactor + leftmostField.get_position().x, leftmostField.get_position().y)
	field.set_position(position)
	$FieldContainer.add_child(field)
	
	rightmostField.connect("loser_found", field, "set_team_1")
	leftmostField.connect("winner_found", field, "set_team_2")
	
	var lineWidth = (fieldOffsetFactor - 1) * field.get_size().x
	var line = BasicLine.instance()
	line.scale = Vector2(-lineWidth, 1)
	line.position = Vector2(0, 0)
	leftmostField.add_child(line)
	
	leftmostField = field
	rightmostField.connect("winner_found", self, "set_right_finished")
	leftmostField.connect("winner_found", self, "set_left_finished")
	
	field = TournamentField.instance()
	position = Vector2(field.get_size().x * fieldOffsetFactor + rightmostField.get_position().x, rightmostField.get_position().y)
	field.set_position(position)
	$FieldContainer.add_child(field)
	
	rightmostField.connect("winner_found", field, "set_team_1")
	leftmostField.connect("winner_found", field, "set_team_2")
	var result = Vector2(leftmostField.get_position().x, position.x + field.get_size().x * fieldOffsetFactor)
	
	field.connect("winner_found", self, "finish_tournament")
	
	line = BasicLine.instance()
	line.scale = Vector2(-lineWidth, 1)
	line.position = Vector2(0, 0)
	field.add_child(line)
	
	return result

func calc_ko_max_level():
	var i = 0
	while globals.teams.size() > pow(2, i):
		i += 1
	return i - 1
	
func _process(delta):
#	if scrollingOn:
	var maxScrollbarScroll = windowSize.x - $ScrollBar.scale.x
	var maxFieldScroll = abs(koSystemWidth - windowSize.x)
	var change = scrollSpeed * delta
	if Input.is_action_pressed("ui_right"):
		var newScrollX = $ScrollBar.position.x + change
		$ScrollBar.position.x = min(newScrollX, boundsX.y - $ScrollBar.scale.x)
				
	elif Input.is_action_pressed("ui_left"):
		var newScrollX = $ScrollBar.position.x - change
		$ScrollBar.position.x = max(newScrollX, boundsX.x)
		
func set_left_finished(winner):
	leftSideFinished = true
	
func set_right_finished(winner):
	rightSideFinished = true
	
func finish_tournament(winner):
	if leftSideFinished and rightSideFinished:
		globals.tournamentWinner = winner
		var error = get_tree().change_scene("res://tournament/WinnerAnnouncement.tscn")
		if (error):
			print("Error on switch to Start: " + error)
