extends Node2D

const Team = preload("res://tournamentLoader/Team.gd")

signal winner_found(winner)
signal loser_found(loser)

var team1 = Team.new()
var team2 = Team.new()
var winner
var loser


func set_team_1(team):
	team1 = team
	$Team1.text = team1.name
	$Team1Players/Label.text = team1.attack.name + " & " + team1.defense.name
	if $Team1Winner.pressed:
		_on_Team1Winner_pressed()
	
func set_team_2(team):
	team2 = team
	$Team2.text = team2.name
	$Team2Players/Label.text = team2.attack.name + " & " + team2.defense.name
	if $Team2Winner.pressed:
		_on_Team2Winner_pressed()

func _on_Team1Winner_pressed():
	if team1.name:
		$Team1Winner.pressed = true
		$Team2Winner.pressed = false
		winner = team1
		loser = team2
		emit_signal("winner_found", winner)
		emit_signal("loser_found", loser)
	else:
		$Team1Winner.pressed = false


func _on_Team2Winner_pressed():
	if team2.name:
		$Team1Winner.pressed = false
		$Team2Winner.pressed = true
		winner = team2
		loser = team1
		emit_signal("winner_found", winner)
		emit_signal("loser_found", loser)
	else:
		$Team2Winner.pressed = false
	

func get_position() -> Vector2:
	return self.position
	

func set_position(pos):
	self.position = pos
	

func get_size() -> Vector2:
	return Vector2(320, 100)

func _on_Team1_mouse_entered():
	if team1.name:
		$Team1Players.visible = true


func _on_Team1_mouse_exited():
	$Team1Players.visible = false
	

func _on_Team2_mouse_entered():
	if team2.name:
		$Team2Players.visible = true


func _on_Team2_mouse_exited():
	$Team2Players.visible = false


func _on_TournamentField_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if event.pressed:
			var url = globals.serverAddress + "/api/games"
			var headers = ["Content-Type: application/json"]
			var players = []
			players.push_back(team1.attack)
			players.push_back(team1.defense)
			players.push_back(team2.attack)
			players.push_back(team2.defense)
			var game = {"players": players}
			var json = to_json(game)
			for player in players:
				print(player.name)
			$PutGame.request(url, headers, false, HTTPClient.METHOD_PUT, json)
