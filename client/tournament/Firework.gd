extends RigidBody2D

var windowSize
var flyingDegree
var goalPosition = Vector2()
# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	windowSize = Vector2(ProjectSettings.get_setting("display/window/size/width"), ProjectSettings.get_setting("display/window/size/height"))
	$AnimationPlayer.stop()
	flyingDegree = randf() * 2 * PI / 6 - PI/6
	goalPosition = Vector2(randf() * windowSize.x, 0)
	self.apply_impulse(Vector2(0, 0), Vector2(0, -1000).rotated(flyingDegree))
	$Sprite.rotate(flyingDegree)
	$StartPlayer.play(0)

func playAnimation():
	$StartPlayer.stop()
	$AnimationPlayer.play("FireworkAnimantion")
	self.gravity_scale = 10

func _on_AnimationPlayer_animation_finished(anim_name):
	self.get_parent().remove_child(self)

func _on_Firework_body_entered(body):
	playAnimation()

func setPosition(newPos):
	self.position = newPos

func setScale(newScale):
	$Sprite.scale = newScale
	
#func look_follow(state, current_transform, target_position):
#    var up_dir = Vector2(0, 1)
#    var cur_dir = current_transform.basis_xform(Vector2(0, 1))
#    var target_dir = (target_position - current_transform.origin).normalized()
#    var rotation_angle = acos(cur_dir.x) - acos(target_dir.x)
#    state.set_angular_velocity(rotation_angle / state.get_step())
#
#func _integrate_forces(state):
##    var target_position = $my_target_spatial_node.get_global_transform().origin
#    look_follow(state, get_global_transform(), goalPosition)
