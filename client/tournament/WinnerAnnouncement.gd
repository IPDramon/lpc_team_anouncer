extends Area2D

const Firework = preload("res://tournament/Firework.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	$FireworkTimer.start()

func _on_FireworkTimer_timeout():

	$FireworkPath/FireworkSpawn.set_offset(randi())

	var firework = Firework.instance()
	var newScale = Vector2(2,2)
	firework.setScale(newScale)
	var spawnPosition = $FireworkPath/FireworkSpawn.position
	firework.setPosition(spawnPosition)
	add_child(firework)


func _on_Area2D_body_exited(body):
	body.playAnimation()
