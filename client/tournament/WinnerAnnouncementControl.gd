extends MarginContainer

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	if globals.tournamentWinner:
		$VBoxContainer/WinnerTeamName.text = globals.tournamentWinner.name
		$VBoxContainer/WinnerNames.text = globals.tournamentWinner.attack.name + " & " + globals.tournamentWinner.defense.name
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_TextureTextButton_pressed():
	var error = get_tree().change_scene("res://mainmenu/MainMenu.tscn")
	if (error):
		print("Error on switch to MainMenu: " + error)
