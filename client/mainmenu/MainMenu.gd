extends MarginContainer

func _ready():
	var dir = Directory.new()
	if not dir.dir_exists(globals.serverAddressDir):
		dir.make_dir_recursive(globals.serverAddressDir)
	var saveFile = File.new()
	if saveFile.file_exists(globals.serverAddressFilePath):
		saveFile.open(globals.serverAddressFilePath, File.READ)
		var saveFileContent = ""
		while not saveFile.eof_reached():
			saveFileContent = saveFileContent + saveFile.get_line()
		saveFile.close()
		globals.serverAddress = saveFileContent

func _on_SettingsButton_pressed():
	var error = get_tree().change_scene("res://settings/Settings.tscn")
	if (error):
		print("Error on switch to Settings: " + error)


func _on_StartButton_pressed():
	var error = get_tree().change_scene("res://tournamentLoader/TournamentLoaderSettings.tscn")
#	var error = get_tree().change_scene("res://tournament/WinnerAnnouncement.tscn")
	if (error):
		print("Error on switch to Start: " + error)
