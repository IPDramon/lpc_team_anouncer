extends TextureButton

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export(String) var text = "Something"

# Called when the node enters the scene tree for the first time.
func _ready():
	$Label.text = text
	$Label.rect_scale = self.rect_scale

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
